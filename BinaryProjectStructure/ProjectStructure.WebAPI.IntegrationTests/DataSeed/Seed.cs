﻿using Bogus;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class Seed
    {
        private const int ENTITY_TEAMS_COUNT = 14;
        private const int ENTITY_USERS_COUNT = 80;
        private const int ENTITY_PROJECT_COUNT = 30;
        private const int ENTITY_TASKS_COUNT = 120;
        private const int PROBABILITY_OF_NULL = 10;

        public Seed()
        {
            GenerateRandomTeams();
            GenerateRandomUsers();
            GenerateRandomProjects();
            GenerateRandomTasks();
        }

        public List<Team> Teams { get; private set; }
        public List<User> Users { get; private set; }
        public List<Project> Projects { get; private set; }
        public List<Tasks> Tasks { get; private set; }


        public void GenerateRandomTeams()
        {
            Faker<Team> teamsFake = new Faker<Team>()
                .RuleFor(t => t.Name, f => f.Name.LastName() + ", " + f.Name.LastName() + " and " + f.Name.LastName())
                .RuleFor(t => t.CreatedAt, f => f.Date.Past(1));

            Teams = teamsFake.Generate(ENTITY_TEAMS_COUNT);
        }

        public void GenerateRandomUsers()
        {
            Faker<User> usersFake = new Faker<User>()
                .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                .RuleFor(u => u.LastName, f => f.Name.LastName())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Past(2))
                .RuleFor(u => u.BirthDay, f => f.Date.Between(f.Date.Past(40), f.Date.Past(8)))
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.TeamId, f => f.Random.Number(1, ENTITY_TEAMS_COUNT));

            Users = usersFake.Generate(ENTITY_USERS_COUNT);
        }

        public void GenerateRandomProjects()
        {
            DateTime startDateCreatedAtProject = new(2020, 1, 1, 1, 1, 1);
            DateTime endDateCreatedAtProject = new(2021, 3, 1, 1, 1, 1);

            Faker<Project> projectFake = new Faker<Project>()
                .RuleFor(p => p.AuthorId, f => f.Random.Number(1, ENTITY_USERS_COUNT))
                .RuleFor(p => p.TeamId, f => f.Random.Number(1, ENTITY_TEAMS_COUNT))
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(7))
                .RuleFor(p => p.Description, f => f.Lorem.Text())
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(startDateCreatedAtProject, endDateCreatedAtProject))
                .RuleFor(p => p.Deadline, f => f.Date.Between(endDateCreatedAtProject, f.Date.Future(1)));

            Projects = projectFake.Generate(ENTITY_PROJECT_COUNT);
        }

        public void GenerateRandomTasks()
        {
            DateTime startDateCreatedAtTask = new(2021, 1, 1, 1, 1, 1);
            DateTime endDateCreatedAtTask = new(2021, 5, 1, 1, 1, 1);
            DateTime startDateFinishedAtTask = new(2021, 6, 1, 1, 1, 1);
            DateTime endDateFinishedAtTask = new(2021, 7, 1, 1, 1, 1);

            Faker<Tasks> tasksFake = new Faker<Tasks>()
                .RuleFor(t => t.ProjectId, f => f.Random.Number(1, ENTITY_PROJECT_COUNT))
                .RuleFor(t => t.PerformerId, f => f.Random.Number(1, ENTITY_USERS_COUNT))
                .RuleFor(t => t.Name, f => f.Lorem.Sentence(7))
                .RuleFor(t => t.Description, f => f.Lorem.Text())
                .RuleFor(t => t.State, f => f.PickRandom<TaskState>())
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(startDateCreatedAtTask, endDateCreatedAtTask))
                .RuleFor(t => t.FinishedAt, f => GetRandomNumberForNullable() < PROBABILITY_OF_NULL ? f.Date.Between(startDateFinishedAtTask, endDateFinishedAtTask) : null);

            Tasks = tasksFake.Generate(ENTITY_TASKS_COUNT);
        }

        private int GetRandomNumberForNullable()
        {
            Random random = new();
            return random.Next(0, 100);
        }
    }
}
