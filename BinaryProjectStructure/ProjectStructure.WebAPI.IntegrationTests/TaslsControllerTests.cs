﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TaslsControllerTests
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private HttpClient _client;

        public TaslsControllerTests()
        {
            WebApplicationFactory<Startup> factory = new WebApplicationFactory<Startup>().WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    var dbDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectContext>));

                    services.Remove(dbDescriptor);

                    services.AddDbContext<ProjectContext>(options =>
                    {
                        options.UseInMemoryDatabase("TasksTest");
                    });
                });
            });
            var dbContext = factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectContext>();
            dbContext.Database.EnsureDeleted();
            Seed seed = new();
            dbContext.Tasks.AddRange(seed.Tasks);
            dbContext.SaveChanges();

            _factory = factory;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task DeleteTask_ThanCheced_Positive()
        {
            //Arrange
            int id = 2;

            //Act
            var response = await _client.DeleteAsync("/api/tasks/" + id);
            response.EnsureSuccessStatusCode();

            var actual = await _client.GetAsync("/api/tasks/" + id);

            _client.Dispose();

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, actual.StatusCode);
        }

        [Fact]
        public async Task DeleteUserWhithInCorrectId_ThanCheced_NotFound()
        {
            //Arrange
            int shift = 1;
            var tasks = await _client.GetAsync("/api/tasks");
            tasks.EnsureSuccessStatusCode();

            var incorrectId = JsonConvert.DeserializeObject<List<TasksDTO>>(await tasks.Content.ReadAsStringAsync()).Max(x => x.Id) + shift;

            //Act
            var response = await _client.DeleteAsync("/api/tasks/" + incorrectId);

            _client.Dispose();

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}

