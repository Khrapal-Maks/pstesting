﻿using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;

namespace ProjectStructure.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ProjectContext _db;

        private GenericRepository<Project> _projectRepository;
        private GenericRepository<Tasks> _taskRepository;
        private GenericRepository<Team> _teamRepository;
        private GenericRepository<User> _userRepository;

        public UnitOfWork(ProjectContext context)
        {
            _db = context ?? throw new ArgumentNullException(nameof(context));
        }

        public GenericRepository<Project> GetRepositoryProjects
        {
            get
            {
                if (_projectRepository == null)
                {
                    _projectRepository = new GenericRepository<Project>(_db);
                }

                return _projectRepository;
            }
        }

        public GenericRepository<Tasks> GetRepositoryTasks
        {
            get
            {
                if (_taskRepository == null)
                {
                    _taskRepository = new GenericRepository<Tasks>(_db);
                }

                return _taskRepository;
            }
        }
        public GenericRepository<Team> GetRepositoryTeams
        {
            get
            {
                if (_teamRepository == null)
                {
                    _teamRepository = new GenericRepository<Team>(_db);
                }

                return _teamRepository;
            }
        }

        public GenericRepository<User> GetRepositoryUsers
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new GenericRepository<User>(_db);
                }

                return _userRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
