﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly ProjectContext _db;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ProjectContext context)
        {
            _db = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = context.Set<TEntity>();
        }

        /// <summary>  
        /// generic method to fetch all the records from db  
        /// </summary>  
        /// <returns></returns>  
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        /// <summary>  
        /// Generic get method on the basis of id for Entities.  
        /// </summary>  
        /// <param name="id"></param>  
        /// <returns></returns>  
        public TEntity Get(object id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>  
        /// generic Create method for the entities  
        /// </summary>  
        /// <param name="entity"></param>  
        public TEntity Create(TEntity entity)
        {
            return _dbSet.Add(entity).Entity;
        }

        /// <summary>  
        /// Generic update method for the entities  
        /// </summary>  
        /// <param name="entityToUpdate"></param>  
        public void Update(object id, TEntity entityToUpdate)
        {
            var trackedProject = _dbSet.Find(id);

            _db.Entry(trackedProject).CurrentValues.SetValues(entityToUpdate);
        }

        /// <summary>  
        /// Generic Delete method for the entities  
        /// </summary>  
        /// <param name="id"></param>  
        public void Delete(object id)
        {
            var entity = _dbSet.Find(id);

            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        /// <summary>  
        /// generic Find method for the entities  
        /// </summary>  
        /// <param name="where"></param>  
        /// <returns></returns>  
        public IEnumerable<TEntity> FindIEnumerable(Func<TEntity, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }
    }
}
