﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(object id);
        IEnumerable<T> FindIEnumerable(Func<T, bool> predicate);
        T Create(T item);
        void Update(object id, T item);
        void Delete(object id);
    }
}
