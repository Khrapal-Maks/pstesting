﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CuratorProjectID = table.Column<int>(type: "int", nullable: true),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Projects_Users_CuratorProjectID",
                        column: x => x.CuratorProjectID,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectId = table.Column<int>(type: "int", nullable: false),
                    PerformerId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 9, 11, 8, 8, 47, 59, DateTimeKind.Local).AddTicks(6563), "Emard, Conn and Hessel" },
                    { 2, new DateTime(2020, 10, 3, 4, 30, 7, 228, DateTimeKind.Local).AddTicks(702), "Walker, Legros and Abshire" },
                    { 3, new DateTime(2021, 4, 26, 20, 8, 10, 298, DateTimeKind.Local).AddTicks(4003), "Hartmann, Bartell and Terry" },
                    { 4, new DateTime(2021, 1, 12, 17, 28, 0, 232, DateTimeKind.Local).AddTicks(7627), "DuBuque, O'Conner and Haley" },
                    { 5, new DateTime(2020, 8, 5, 8, 27, 0, 44, DateTimeKind.Local).AddTicks(3033), "Larkin, Jerde and Grant" },
                    { 6, new DateTime(2020, 10, 14, 5, 14, 52, 232, DateTimeKind.Local).AddTicks(3572), "Feest, Emard and Stokes" },
                    { 7, new DateTime(2020, 12, 5, 20, 10, 56, 334, DateTimeKind.Local).AddTicks(4904), "Bahringer, Bins and Padberg" },
                    { 8, new DateTime(2020, 10, 11, 5, 31, 49, 80, DateTimeKind.Local).AddTicks(385), "Langosh, Ledner and Steuber" },
                    { 9, new DateTime(2020, 12, 8, 15, 23, 11, 384, DateTimeKind.Local).AddTicks(4426), "Homenick, Bode and Schulist" },
                    { 10, new DateTime(2021, 5, 29, 7, 27, 7, 382, DateTimeKind.Local).AddTicks(8983), "Hamill, Nicolas and Jast" },
                    { 11, new DateTime(2021, 1, 15, 7, 32, 30, 245, DateTimeKind.Local).AddTicks(815), "Nader, Bartell and Toy" },
                    { 12, new DateTime(2021, 6, 8, 3, 16, 11, 102, DateTimeKind.Local).AddTicks(620), "Kuhlman, Kozey and Hegmann" },
                    { 13, new DateTime(2021, 3, 28, 7, 39, 52, 52, DateTimeKind.Local).AddTicks(6305), "Armstrong, Collier and Cruickshank" },
                    { 14, new DateTime(2021, 7, 4, 9, 52, 27, 901, DateTimeKind.Local).AddTicks(8205), "Hills, Zieme and Parker" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 9, new DateTime(2016, 7, 16, 6, 17, 53, 356, DateTimeKind.Local).AddTicks(8486), "Demetris64@yahoo.com", "Maximus", "Luettgen", new DateTime(2021, 1, 5, 22, 56, 42, 445, DateTimeKind.Local).AddTicks(6855), 1 },
                    { 33, new DateTime(2011, 6, 10, 15, 33, 37, 434, DateTimeKind.Local).AddTicks(2158), "Brett_Fay@gmail.com", "Kacie", "Wiza", new DateTime(2021, 6, 26, 23, 15, 28, 839, DateTimeKind.Local).AddTicks(1708), 10 },
                    { 17, new DateTime(2006, 4, 4, 16, 43, 43, 851, DateTimeKind.Local).AddTicks(5687), "Damien_Hyatt@yahoo.com", "Lue", "Brekke", new DateTime(2020, 12, 8, 17, 52, 17, 766, DateTimeKind.Local).AddTicks(8521), 10 },
                    { 16, new DateTime(1987, 11, 19, 17, 25, 4, 702, DateTimeKind.Local).AddTicks(702), "Rhoda_Price@gmail.com", "Werner", "Bashirian", new DateTime(2020, 10, 23, 12, 40, 11, 88, DateTimeKind.Local).AddTicks(1175), 10 },
                    { 45, new DateTime(2014, 12, 7, 16, 31, 25, 964, DateTimeKind.Local).AddTicks(8365), "Gerald.Thiel@yahoo.com", "Hallie", "Halvorson", new DateTime(2021, 5, 28, 7, 26, 50, 987, DateTimeKind.Local).AddTicks(7953), 9 },
                    { 26, new DateTime(2014, 5, 10, 15, 53, 49, 445, DateTimeKind.Local).AddTicks(9180), "Rafael17@gmail.com", "Hilario", "Brekke", new DateTime(2020, 5, 17, 3, 13, 41, 617, DateTimeKind.Local).AddTicks(4636), 9 },
                    { 19, new DateTime(2017, 3, 6, 16, 3, 57, 908, DateTimeKind.Local).AddTicks(4200), "Yasmine1@yahoo.com", "Jarret", "Jakubowski", new DateTime(2019, 12, 14, 22, 54, 29, 991, DateTimeKind.Local).AddTicks(9262), 9 },
                    { 8, new DateTime(2016, 8, 13, 13, 23, 48, 451, DateTimeKind.Local).AddTicks(2292), "Hubert_Abbott@yahoo.com", "Chloe", "Orn", new DateTime(2019, 8, 31, 2, 17, 59, 625, DateTimeKind.Local).AddTicks(687), 9 },
                    { 40, new DateTime(2017, 8, 2, 19, 28, 40, 992, DateTimeKind.Local).AddTicks(501), "Hunter_Mueller@yahoo.com", "Cali", "Ankunding", new DateTime(2021, 4, 27, 11, 33, 37, 74, DateTimeKind.Local).AddTicks(7678), 10 },
                    { 77, new DateTime(1995, 10, 15, 21, 41, 15, 339, DateTimeKind.Local).AddTicks(3741), "Harold20@yahoo.com", "Vivienne", "Dickens", new DateTime(2019, 10, 1, 13, 44, 43, 717, DateTimeKind.Local).AddTicks(4556), 8 },
                    { 49, new DateTime(2017, 1, 11, 23, 58, 43, 258, DateTimeKind.Local).AddTicks(7664), "Clinton_Roob8@hotmail.com", "Kobe", "Stoltenberg", new DateTime(2021, 6, 6, 20, 10, 55, 802, DateTimeKind.Local).AddTicks(1217), 8 },
                    { 41, new DateTime(1996, 4, 5, 17, 24, 37, 98, DateTimeKind.Local).AddTicks(8193), "Tess_Kulas76@gmail.com", "Russell", "Watsica", new DateTime(2021, 1, 7, 18, 38, 56, 497, DateTimeKind.Local).AddTicks(6666), 8 },
                    { 32, new DateTime(2008, 4, 16, 10, 52, 59, 548, DateTimeKind.Local).AddTicks(5666), "Efrain84@hotmail.com", "Christop", "MacGyver", new DateTime(2019, 10, 2, 14, 30, 4, 147, DateTimeKind.Local).AddTicks(1325), 8 },
                    { 27, new DateTime(2019, 2, 22, 4, 3, 41, 169, DateTimeKind.Local).AddTicks(792), "Aliyah.Hyatt@hotmail.com", "Valerie", "Moore", new DateTime(2020, 8, 31, 17, 42, 3, 665, DateTimeKind.Local).AddTicks(4506), 8 },
                    { 18, new DateTime(2001, 9, 20, 6, 7, 50, 200, DateTimeKind.Local).AddTicks(3643), "Maggie_Mosciski@hotmail.com", "Malvina", "Greenfelder", new DateTime(2019, 12, 29, 5, 35, 56, 663, DateTimeKind.Local).AddTicks(2914), 8 },
                    { 2, new DateTime(1992, 12, 8, 2, 21, 43, 22, DateTimeKind.Local).AddTicks(9308), "Concepcion.Hane@gmail.com", "Rod", "Roberts", new DateTime(2021, 3, 12, 22, 57, 24, 725, DateTimeKind.Local).AddTicks(9763), 8 },
                    { 66, new DateTime(2013, 6, 5, 4, 2, 15, 377, DateTimeKind.Local).AddTicks(6050), "Jermey94@hotmail.com", "Michael", "Schumm", new DateTime(2020, 11, 21, 7, 33, 41, 85, DateTimeKind.Local).AddTicks(2211), 7 },
                    { 73, new DateTime(2018, 6, 21, 12, 0, 21, 516, DateTimeKind.Local).AddTicks(8020), "River_Bechtelar@gmail.com", "Kian", "Larson", new DateTime(2020, 3, 15, 16, 33, 34, 516, DateTimeKind.Local).AddTicks(541), 8 },
                    { 51, new DateTime(2013, 1, 28, 2, 14, 46, 592, DateTimeKind.Local).AddTicks(6997), "Ruby0@yahoo.com", "Ernesto", "Swaniawski", new DateTime(2020, 7, 31, 18, 15, 10, 838, DateTimeKind.Local).AddTicks(430), 7 },
                    { 59, new DateTime(2016, 7, 14, 9, 29, 41, 894, DateTimeKind.Local).AddTicks(7003), "Declan_Graham@gmail.com", "Jocelyn", "Auer", new DateTime(2020, 8, 7, 15, 4, 28, 952, DateTimeKind.Local).AddTicks(6906), 10 },
                    { 78, new DateTime(2010, 6, 26, 22, 21, 55, 403, DateTimeKind.Local).AddTicks(317), "Franco23@yahoo.com", "Joy", "Schoen", new DateTime(2020, 3, 22, 19, 24, 23, 110, DateTimeKind.Local).AddTicks(2154), 10 },
                    { 6, new DateTime(2010, 6, 14, 22, 17, 40, 98, DateTimeKind.Local).AddTicks(4245), "Jace44@gmail.com", "Samir", "King", new DateTime(2020, 4, 28, 18, 10, 17, 232, DateTimeKind.Local).AddTicks(752), 14 },
                    { 4, new DateTime(1998, 11, 30, 11, 34, 7, 596, DateTimeKind.Local).AddTicks(1551), "Manley27@yahoo.com", "Keara", "Friesen", new DateTime(2021, 2, 4, 3, 7, 49, 796, DateTimeKind.Local).AddTicks(2144), 14 },
                    { 28, new DateTime(1992, 7, 14, 14, 54, 57, 258, DateTimeKind.Local).AddTicks(9984), "Shana_Mitchell74@yahoo.com", "Barton", "Hintz", new DateTime(2020, 9, 24, 10, 9, 39, 167, DateTimeKind.Local).AddTicks(9815), 13 },
                    { 5, new DateTime(2009, 8, 2, 8, 6, 55, 945, DateTimeKind.Local).AddTicks(1349), "Guadalupe24@hotmail.com", "Edd", "Gislason", new DateTime(2021, 2, 23, 3, 47, 16, 293, DateTimeKind.Local).AddTicks(5502), 13 },
                    { 68, new DateTime(2014, 8, 24, 21, 57, 24, 955, DateTimeKind.Local).AddTicks(9065), "Giovanny.Weimann38@yahoo.com", "Laurianne", "Stoltenberg", new DateTime(2021, 2, 5, 7, 22, 54, 330, DateTimeKind.Local).AddTicks(3955), 12 },
                    { 44, new DateTime(1998, 3, 25, 21, 12, 24, 890, DateTimeKind.Local).AddTicks(8661), "Elijah_Hartmann@gmail.com", "Abigayle", "Gorczany", new DateTime(2020, 11, 25, 6, 19, 44, 977, DateTimeKind.Local).AddTicks(7556), 12 },
                    { 39, new DateTime(2017, 5, 4, 2, 20, 52, 560, DateTimeKind.Local).AddTicks(8003), "Micah96@hotmail.com", "Ricky", "Hills", new DateTime(2020, 1, 10, 21, 21, 35, 910, DateTimeKind.Local).AddTicks(3370), 12 },
                    { 63, new DateTime(2011, 7, 25, 9, 34, 6, 478, DateTimeKind.Local).AddTicks(8320), "Jamel5@hotmail.com", "Kareem", "Anderson", new DateTime(2020, 8, 20, 23, 11, 36, 599, DateTimeKind.Local).AddTicks(61), 10 },
                    { 38, new DateTime(2005, 3, 13, 23, 46, 38, 977, DateTimeKind.Local).AddTicks(2697), "Rosina_Morissette@gmail.com", "Irwin", "Schinner", new DateTime(2021, 3, 8, 20, 49, 10, 269, DateTimeKind.Local).AddTicks(2989), 12 },
                    { 15, new DateTime(2014, 7, 19, 17, 51, 39, 397, DateTimeKind.Local).AddTicks(7139), "Henriette18@gmail.com", "Kirstin", "Lowe", new DateTime(2019, 12, 23, 8, 5, 38, 393, DateTimeKind.Local).AddTicks(2593), 12 },
                    { 3, new DateTime(2013, 8, 4, 1, 15, 49, 374, DateTimeKind.Local).AddTicks(4984), "Gerda.Jacobi67@yahoo.com", "Monroe", "Mitchell", new DateTime(2020, 5, 20, 13, 39, 12, 892, DateTimeKind.Local).AddTicks(3175), 12 },
                    { 71, new DateTime(2003, 7, 16, 10, 38, 30, 79, DateTimeKind.Local).AddTicks(4921), "Dalton_Hackett86@hotmail.com", "Natalie", "Kunze", new DateTime(2020, 7, 6, 21, 6, 13, 934, DateTimeKind.Local).AddTicks(4857), 11 },
                    { 58, new DateTime(2005, 11, 18, 9, 39, 28, 759, DateTimeKind.Local).AddTicks(531), "Layne78@yahoo.com", "Guadalupe", "Littel", new DateTime(2021, 2, 2, 22, 47, 27, 73, DateTimeKind.Local).AddTicks(3853), 11 },
                    { 56, new DateTime(2002, 5, 17, 16, 52, 22, 357, DateTimeKind.Local).AddTicks(8120), "Dolly.Rau89@hotmail.com", "Matilda", "Kuhic", new DateTime(2019, 10, 7, 3, 46, 48, 332, DateTimeKind.Local).AddTicks(1075), 11 },
                    { 12, new DateTime(2012, 4, 29, 12, 50, 47, 435, DateTimeKind.Local).AddTicks(7848), "Turner_Aufderhar62@yahoo.com", "Lucas", "Ankunding", new DateTime(2021, 3, 12, 16, 36, 46, 533, DateTimeKind.Local).AddTicks(7063), 11 },
                    { 10, new DateTime(2015, 6, 30, 20, 58, 37, 686, DateTimeKind.Local).AddTicks(4780), "Janessa60@yahoo.com", "Hollie", "Sauer", new DateTime(2020, 6, 28, 5, 2, 34, 720, DateTimeKind.Local).AddTicks(6418), 11 },
                    { 21, new DateTime(2010, 4, 8, 20, 15, 24, 221, DateTimeKind.Local).AddTicks(6919), "Casey.Daniel84@gmail.com", "Giovanny", "McCullough", new DateTime(2020, 2, 21, 20, 46, 47, 554, DateTimeKind.Local).AddTicks(2160), 12 },
                    { 43, new DateTime(2017, 12, 12, 4, 38, 56, 136, DateTimeKind.Local).AddTicks(7832), "Jamel88@gmail.com", "Cloyd", "Leannon", new DateTime(2021, 4, 15, 5, 45, 52, 72, DateTimeKind.Local).AddTicks(3737), 7 },
                    { 72, new DateTime(1998, 12, 15, 18, 44, 28, 911, DateTimeKind.Local).AddTicks(5535), "Kyle_Stokes@yahoo.com", "Emmanuelle", "Sipes", new DateTime(2020, 11, 18, 4, 21, 14, 535, DateTimeKind.Local).AddTicks(4874), 6 },
                    { 67, new DateTime(1996, 3, 6, 2, 34, 59, 364, DateTimeKind.Local).AddTicks(9266), "Jazmyne.Daniel@hotmail.com", "Marilou", "Nolan", new DateTime(2020, 2, 11, 14, 52, 33, 798, DateTimeKind.Local).AddTicks(5437), 6 },
                    { 60, new DateTime(1997, 6, 28, 15, 39, 17, 142, DateTimeKind.Local).AddTicks(9840), "Deondre88@hotmail.com", "Tavares", "Von", new DateTime(2021, 5, 8, 18, 37, 15, 550, DateTimeKind.Local).AddTicks(4090), 3 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 48, new DateTime(2015, 5, 8, 3, 10, 30, 286, DateTimeKind.Local).AddTicks(3013), "Madie_Bashirian22@yahoo.com", "Camila", "Pagac", new DateTime(2019, 7, 27, 0, 46, 12, 890, DateTimeKind.Local).AddTicks(2804), 3 },
                    { 23, new DateTime(2017, 8, 22, 3, 39, 59, 711, DateTimeKind.Local).AddTicks(8088), "Autumn69@hotmail.com", "Alva", "Bashirian", new DateTime(2020, 3, 14, 17, 38, 55, 898, DateTimeKind.Local).AddTicks(3296), 3 },
                    { 22, new DateTime(2012, 5, 25, 9, 8, 7, 576, DateTimeKind.Local).AddTicks(7004), "Dora84@yahoo.com", "Wilhelmine", "White", new DateTime(2020, 7, 3, 11, 45, 10, 128, DateTimeKind.Local).AddTicks(5879), 3 },
                    { 64, new DateTime(2016, 7, 28, 13, 26, 27, 644, DateTimeKind.Local).AddTicks(7484), "Lisa83@hotmail.com", "Alberto", "Stamm", new DateTime(2019, 7, 18, 21, 57, 50, 617, DateTimeKind.Local).AddTicks(6306), 2 },
                    { 20, new DateTime(2015, 7, 22, 19, 38, 1, 51, DateTimeKind.Local).AddTicks(7241), "Ericka.Cronin@yahoo.com", "Makenna", "Gulgowski", new DateTime(2021, 2, 24, 23, 59, 21, 822, DateTimeKind.Local).AddTicks(3685), 2 },
                    { 14, new DateTime(2011, 4, 15, 12, 22, 3, 881, DateTimeKind.Local).AddTicks(670), "Catalina_Ratke@yahoo.com", "Reynold", "Schoen", new DateTime(2020, 11, 27, 18, 31, 22, 289, DateTimeKind.Local).AddTicks(1637), 2 },
                    { 76, new DateTime(1982, 11, 24, 2, 38, 23, 473, DateTimeKind.Local).AddTicks(7294), "Stan_Yost16@hotmail.com", "Elisabeth", "Crona", new DateTime(2020, 11, 7, 10, 38, 4, 367, DateTimeKind.Local).AddTicks(8871), 3 },
                    { 1, new DateTime(1999, 2, 21, 15, 3, 47, 155, DateTimeKind.Local).AddTicks(6123), "Raheem80@gmail.com", "Duncan", "Nienow", new DateTime(2019, 12, 6, 3, 28, 23, 759, DateTimeKind.Local).AddTicks(1881), 2 },
                    { 62, new DateTime(2006, 9, 11, 6, 41, 8, 896, DateTimeKind.Local).AddTicks(6622), "Stevie_Strosin@hotmail.com", "Pamela", "Borer", new DateTime(2019, 7, 25, 17, 29, 12, 180, DateTimeKind.Local).AddTicks(3006), 1 },
                    { 61, new DateTime(2005, 11, 17, 2, 21, 18, 817, DateTimeKind.Local).AddTicks(3727), "Kali66@gmail.com", "Andy", "Adams", new DateTime(2020, 2, 11, 19, 50, 15, 424, DateTimeKind.Local).AddTicks(1097), 1 },
                    { 57, new DateTime(2020, 2, 28, 14, 27, 54, 735, DateTimeKind.Local).AddTicks(9072), "Cornelius_OKeefe90@gmail.com", "Rahsaan", "Marks", new DateTime(2020, 5, 30, 3, 4, 18, 888, DateTimeKind.Local).AddTicks(9735), 1 },
                    { 53, new DateTime(2013, 11, 1, 9, 32, 52, 929, DateTimeKind.Local).AddTicks(9978), "Merritt_Sawayn@gmail.com", "Godfrey", "Metz", new DateTime(2021, 5, 27, 23, 43, 46, 227, DateTimeKind.Local).AddTicks(911), 1 },
                    { 47, new DateTime(2019, 5, 5, 5, 0, 46, 898, DateTimeKind.Local).AddTicks(7766), "Laury68@gmail.com", "Mac", "Little", new DateTime(2019, 12, 6, 11, 39, 29, 724, DateTimeKind.Local).AddTicks(3138), 1 },
                    { 35, new DateTime(1990, 6, 25, 20, 27, 17, 632, DateTimeKind.Local).AddTicks(7509), "Kelli_Fadel@hotmail.com", "Teagan", "Kshlerin", new DateTime(2020, 8, 17, 5, 28, 21, 580, DateTimeKind.Local).AddTicks(8599), 1 },
                    { 24, new DateTime(1996, 10, 12, 13, 54, 17, 791, DateTimeKind.Local).AddTicks(1295), "Sigmund64@hotmail.com", "Angel", "Goodwin", new DateTime(2021, 1, 29, 20, 17, 39, 938, DateTimeKind.Local).AddTicks(8194), 1 },
                    { 75, new DateTime(2009, 6, 20, 22, 1, 18, 847, DateTimeKind.Local).AddTicks(1756), "Amie_Stokes@hotmail.com", "Alva", "Ryan", new DateTime(2020, 9, 12, 0, 19, 32, 477, DateTimeKind.Local).AddTicks(7873), 1 },
                    { 31, new DateTime(1985, 7, 10, 19, 39, 8, 866, DateTimeKind.Local).AddTicks(464), "Amira_Padberg63@gmail.com", "Elinore", "Tremblay", new DateTime(2021, 5, 14, 1, 29, 40, 930, DateTimeKind.Local).AddTicks(2619), 4 },
                    { 65, new DateTime(2019, 5, 20, 23, 19, 2, 737, DateTimeKind.Local).AddTicks(3810), "Judson_Emard@gmail.com", "Annetta", "Skiles", new DateTime(2019, 8, 21, 23, 39, 57, 387, DateTimeKind.Local).AddTicks(1470), 4 },
                    { 74, new DateTime(2018, 1, 14, 1, 11, 28, 84, DateTimeKind.Local).AddTicks(7389), "Trisha10@yahoo.com", "Tre", "Heaney", new DateTime(2019, 8, 25, 22, 2, 17, 345, DateTimeKind.Local).AddTicks(7284), 4 },
                    { 55, new DateTime(2019, 4, 18, 22, 42, 22, 384, DateTimeKind.Local).AddTicks(4098), "Timmy86@gmail.com", "Golden", "Hilll", new DateTime(2021, 3, 24, 4, 35, 9, 642, DateTimeKind.Local).AddTicks(6550), 6 },
                    { 54, new DateTime(2015, 12, 4, 19, 20, 6, 314, DateTimeKind.Local).AddTicks(1273), "Leonie38@yahoo.com", "Mustafa", "Kuphal", new DateTime(2019, 7, 21, 8, 2, 48, 422, DateTimeKind.Local).AddTicks(6229), 6 },
                    { 50, new DateTime(2014, 7, 21, 14, 55, 44, 296, DateTimeKind.Local).AddTicks(7345), "Celestino.Lesch32@gmail.com", "Axel", "Schultz", new DateTime(2020, 11, 20, 4, 50, 21, 653, DateTimeKind.Local).AddTicks(1773), 6 },
                    { 42, new DateTime(2016, 12, 1, 6, 29, 52, 883, DateTimeKind.Local).AddTicks(3154), "Rita.Hayes@yahoo.com", "Fernando", "Ratke", new DateTime(2019, 8, 11, 16, 56, 9, 912, DateTimeKind.Local).AddTicks(1755), 6 },
                    { 37, new DateTime(1996, 3, 27, 3, 18, 50, 233, DateTimeKind.Local).AddTicks(5314), "Houston_Legros28@gmail.com", "Patsy", "Hintz", new DateTime(2021, 1, 4, 20, 14, 10, 424, DateTimeKind.Local).AddTicks(4257), 6 },
                    { 30, new DateTime(2015, 8, 13, 8, 58, 15, 626, DateTimeKind.Local).AddTicks(5596), "Wilber19@hotmail.com", "Korey", "Hayes", new DateTime(2019, 10, 5, 8, 11, 40, 113, DateTimeKind.Local).AddTicks(7021), 6 },
                    { 29, new DateTime(2008, 3, 11, 15, 4, 13, 673, DateTimeKind.Local).AddTicks(5109), "Tanner.Olson@yahoo.com", "Karen", "Hansen", new DateTime(2020, 4, 30, 8, 8, 52, 872, DateTimeKind.Local).AddTicks(4347), 6 },
                    { 69, new DateTime(2015, 1, 22, 13, 28, 7, 113, DateTimeKind.Local).AddTicks(2192), "Silas_Koepp@hotmail.com", "Marco", "Gusikowski", new DateTime(2021, 5, 29, 16, 5, 23, 152, DateTimeKind.Local).AddTicks(6988), 5 },
                    { 52, new DateTime(2010, 6, 1, 17, 30, 15, 34, DateTimeKind.Local).AddTicks(216), "Art_Dooley@gmail.com", "Maude", "Heller", new DateTime(2020, 1, 7, 19, 5, 29, 823, DateTimeKind.Local).AddTicks(9727), 5 },
                    { 46, new DateTime(2001, 2, 27, 2, 29, 4, 903, DateTimeKind.Local).AddTicks(9906), "Brant.Simonis@yahoo.com", "Anissa", "Senger", new DateTime(2021, 2, 27, 4, 12, 48, 62, DateTimeKind.Local).AddTicks(560), 5 },
                    { 34, new DateTime(2016, 2, 5, 23, 21, 33, 260, DateTimeKind.Local).AddTicks(2397), "Milan44@hotmail.com", "Monserrate", "Morar", new DateTime(2020, 3, 15, 2, 23, 31, 696, DateTimeKind.Local).AddTicks(5995), 5 },
                    { 25, new DateTime(2013, 7, 27, 6, 32, 45, 706, DateTimeKind.Local).AddTicks(6451), "Joana67@gmail.com", "Theresia", "Borer", new DateTime(2020, 12, 16, 19, 33, 57, 958, DateTimeKind.Local).AddTicks(4867), 5 },
                    { 13, new DateTime(1998, 2, 22, 9, 42, 8, 912, DateTimeKind.Local).AddTicks(6009), "Valerie.Kulas77@yahoo.com", "Desmond", "Schuster", new DateTime(2021, 4, 7, 15, 49, 49, 535, DateTimeKind.Local).AddTicks(6477), 5 },
                    { 11, new DateTime(2013, 8, 1, 4, 4, 13, 904, DateTimeKind.Local).AddTicks(1563), "Murphy.Kulas@hotmail.com", "Quincy", "Torp", new DateTime(2021, 5, 30, 10, 49, 27, 689, DateTimeKind.Local).AddTicks(5660), 5 },
                    { 7, new DateTime(2017, 1, 5, 20, 34, 15, 35, DateTimeKind.Local).AddTicks(5758), "Freeda.Green@yahoo.com", "Antwan", "Jerde", new DateTime(2021, 1, 7, 20, 39, 20, 873, DateTimeKind.Local).AddTicks(2994), 5 },
                    { 80, new DateTime(2016, 3, 14, 22, 30, 32, 994, DateTimeKind.Local).AddTicks(8983), "Bridgette.Cartwright@hotmail.com", "Adalberto", "Gerlach", new DateTime(2021, 3, 8, 18, 7, 59, 300, DateTimeKind.Local).AddTicks(2582), 4 },
                    { 79, new DateTime(2010, 11, 30, 19, 57, 58, 279, DateTimeKind.Local).AddTicks(726), "Aracely5@gmail.com", "Madge", "Anderson", new DateTime(2021, 3, 19, 16, 38, 48, 444, DateTimeKind.Local).AddTicks(5779), 4 },
                    { 36, new DateTime(2001, 1, 5, 12, 3, 30, 139, DateTimeKind.Local).AddTicks(8774), "Lola76@gmail.com", "Alejandrin", "Green", new DateTime(2019, 9, 9, 11, 0, 17, 425, DateTimeKind.Local).AddTicks(5799), 14 },
                    { 70, new DateTime(1991, 1, 4, 3, 19, 24, 330, DateTimeKind.Local).AddTicks(7895), "Haley.Vandervort@yahoo.com", "Kaylin", "Treutel", new DateTime(2021, 5, 30, 7, 11, 4, 936, DateTimeKind.Local).AddTicks(2348), 14 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 3, 24, new DateTime(2020, 8, 10, 20, 50, 14, 203, DateTimeKind.Unspecified).AddTicks(4744), new DateTime(2021, 9, 24, 0, 3, 18, 963, DateTimeKind.Unspecified).AddTicks(7177), "Accusamus ut maiores cupiditate nulla eligendi delectus. Dolorem ut consequatur. Non et fugiat. Corrupti ratione placeat esse. Quia laborum sint.", "Placeat qui asperiores non sit deleniti et.", 6 },
                    { 1, 6, new DateTime(2020, 10, 18, 8, 55, 19, 150, DateTimeKind.Unspecified).AddTicks(7443), new DateTime(2021, 3, 28, 3, 11, 0, 124, DateTimeKind.Unspecified).AddTicks(2417), "eos", "Consectetur quasi amet et exercitationem et hic.", 6 },
                    { 12, 5, new DateTime(2021, 1, 19, 22, 47, 47, 708, DateTimeKind.Unspecified).AddTicks(2085), new DateTime(2021, 7, 7, 17, 36, 31, 257, DateTimeKind.Unspecified).AddTicks(9514), "vel", "Dolores molestias eveniet ullam eaque incidunt vitae.", 10 },
                    { 28, 39, new DateTime(2020, 5, 23, 4, 13, 26, 985, DateTimeKind.Unspecified).AddTicks(6191), new DateTime(2021, 5, 6, 18, 37, 54, 698, DateTimeKind.Unspecified).AddTicks(1429), "Et dolores dolor voluptatem reiciendis.\nConsequatur temporibus quam corrupti saepe.", "Voluptatum esse qui est sint eum labore.", 4 },
                    { 11, 39, new DateTime(2020, 8, 13, 14, 31, 28, 854, DateTimeKind.Unspecified).AddTicks(3475), new DateTime(2021, 5, 12, 21, 24, 11, 829, DateTimeKind.Unspecified).AddTicks(7393), "Corrupti quo alias at sed quia sed. Est dolor dicta assumenda ut error quas provident non est. Ut sapiente laudantium molestiae velit quasi nulla magni doloribus saepe. Laborum et neque recusandae expedita.", "Alias maiores autem ipsam iusto et et.", 2 },
                    { 23, 38, new DateTime(2020, 10, 7, 8, 40, 16, 792, DateTimeKind.Unspecified).AddTicks(6128), new DateTime(2021, 5, 23, 4, 12, 9, 834, DateTimeKind.Unspecified).AddTicks(5075), "Ut magni quas praesentium neque quos labore impedit sapiente.\nSint omnis quis nulla necessitatibus consequuntur sint.\nMolestiae quia libero.\nEa recusandae qui.\nInventore a rerum tenetur magni sit.", "Magnam ea ut cumque dicta sit consequatur.", 9 },
                    { 20, 12, new DateTime(2020, 4, 21, 1, 54, 9, 131, DateTimeKind.Unspecified).AddTicks(2907), new DateTime(2021, 12, 13, 0, 18, 21, 247, DateTimeKind.Unspecified).AddTicks(5696), "Ut vel earum consequuntur qui in praesentium amet in.\nAd consequatur et modi rerum delectus et eos dolores.\nSed veniam eius consequatur cumque qui facere ipsum ut.\nCumque qui iure.\nQuasi quis beatae et et ad enim.\nCommodi corporis consequatur earum quo saepe deserunt eum eius.", "Placeat suscipit fugit sed aliquid fugiat in.", 2 },
                    { 4, 78, new DateTime(2020, 6, 30, 4, 37, 30, 754, DateTimeKind.Unspecified).AddTicks(2522), new DateTime(2021, 3, 25, 11, 23, 39, 484, DateTimeKind.Unspecified).AddTicks(2452), "minus", "Iusto dolorum aut velit libero perferendis sed.", 6 },
                    { 2, 17, new DateTime(2020, 2, 20, 21, 17, 29, 528, DateTimeKind.Unspecified).AddTicks(9040), new DateTime(2021, 4, 2, 14, 6, 7, 644, DateTimeKind.Unspecified).AddTicks(5462), "Ut enim ad aut provident.\nEst sint enim voluptas.\nNon rerum iusto.\nQuaerat enim dolor.", "Quod magni enim modi placeat quam non.", 3 },
                    { 25, 77, new DateTime(2020, 7, 3, 14, 3, 26, 144, DateTimeKind.Unspecified).AddTicks(7854), new DateTime(2021, 8, 22, 18, 2, 0, 286, DateTimeKind.Unspecified).AddTicks(8288), "quod", "Est magni natus dolor sed est similique.", 2 },
                    { 14, 49, new DateTime(2021, 1, 15, 23, 33, 10, 819, DateTimeKind.Unspecified).AddTicks(882), new DateTime(2021, 7, 27, 20, 52, 45, 334, DateTimeKind.Unspecified).AddTicks(5180), "Consequuntur quis voluptas omnis ut dolor maiores.", "Voluptates aut voluptatem itaque quisquam aliquid fugiat.", 14 },
                    { 5, 32, new DateTime(2021, 2, 9, 17, 38, 36, 286, DateTimeKind.Unspecified).AddTicks(5143), new DateTime(2021, 6, 21, 13, 41, 29, 398, DateTimeKind.Unspecified).AddTicks(7655), "Et corrupti nam. Quasi alias eos voluptatem alias. Qui minima ex nam eos tempora exercitationem consectetur. Quisquam expedita voluptatem expedita ut tempora necessitatibus at quo. Officia qui ducimus sed non ipsum dolor aliquid magnam.", "Iure accusantium tempore accusamus placeat autem ut.", 14 },
                    { 8, 55, new DateTime(2020, 4, 1, 8, 37, 30, 436, DateTimeKind.Unspecified).AddTicks(9670), new DateTime(2022, 2, 25, 2, 10, 4, 113, DateTimeKind.Unspecified).AddTicks(3500), "sed", "Quae ut accusantium iusto quisquam odit sint.", 13 },
                    { 16, 37, new DateTime(2020, 8, 12, 14, 56, 52, 839, DateTimeKind.Unspecified).AddTicks(1217), new DateTime(2021, 8, 13, 18, 18, 47, 134, DateTimeKind.Unspecified).AddTicks(75), "sed", "Aspernatur blanditiis quis numquam earum excepturi quis.", 7 },
                    { 15, 69, new DateTime(2020, 11, 16, 22, 28, 51, 349, DateTimeKind.Unspecified).AddTicks(7770), new DateTime(2021, 10, 9, 11, 12, 31, 922, DateTimeKind.Unspecified).AddTicks(3042), "Minus est et fugit voluptatem voluptas.", "Harum sed autem quos dolor blanditiis et.", 6 },
                    { 29, 52, new DateTime(2020, 7, 9, 7, 18, 13, 657, DateTimeKind.Unspecified).AddTicks(1734), new DateTime(2021, 5, 7, 17, 33, 38, 447, DateTimeKind.Unspecified).AddTicks(5387), "Animi quia veniam.\nMollitia magni ut facere illum.\nEos provident sint.", "Eligendi doloribus eos in laboriosam nisi distinctio.", 9 },
                    { 18, 52, new DateTime(2020, 9, 2, 7, 48, 14, 798, DateTimeKind.Unspecified).AddTicks(6821), new DateTime(2021, 5, 27, 7, 14, 43, 918, DateTimeKind.Unspecified).AddTicks(9885), "fugiat", "Suscipit incidunt molestiae reprehenderit eum autem veniam.", 9 },
                    { 24, 46, new DateTime(2020, 1, 15, 20, 9, 3, 580, DateTimeKind.Unspecified).AddTicks(2215), new DateTime(2021, 7, 17, 2, 1, 50, 767, DateTimeKind.Unspecified).AddTicks(469), "corrupti", "Exercitationem pariatur placeat nihil voluptate est sint.", 6 },
                    { 7, 79, new DateTime(2020, 3, 10, 0, 47, 42, 127, DateTimeKind.Unspecified).AddTicks(1250), new DateTime(2021, 7, 18, 15, 19, 1, 793, DateTimeKind.Unspecified).AddTicks(9776), "veniam", "Qui veritatis dolor voluptatem ullam ea velit.", 12 },
                    { 13, 65, new DateTime(2020, 6, 29, 13, 29, 2, 658, DateTimeKind.Unspecified).AddTicks(2099), new DateTime(2021, 7, 22, 13, 22, 36, 86, DateTimeKind.Unspecified).AddTicks(279), "odit", "Omnis voluptatem velit officia dolor ad consectetur.", 5 },
                    { 6, 76, new DateTime(2020, 4, 9, 18, 53, 56, 939, DateTimeKind.Unspecified).AddTicks(2040), new DateTime(2021, 8, 23, 13, 38, 54, 529, DateTimeKind.Unspecified).AddTicks(4924), "dolor", "Quod non aut tempore consequatur consequatur occaecati.", 3 },
                    { 10, 60, new DateTime(2020, 5, 22, 18, 4, 46, 765, DateTimeKind.Unspecified).AddTicks(2628), new DateTime(2021, 9, 10, 22, 53, 46, 180, DateTimeKind.Unspecified).AddTicks(3812), "Impedit officiis repudiandae laboriosam rerum.\nCorporis totam sunt ut sed architecto eum molestiae nemo sit.\nVero praesentium dignissimos ducimus qui consequatur vel magni ipsum delectus.", "Molestiae rerum dolore molestias voluptas facilis aperiam.", 8 },
                    { 21, 23, new DateTime(2020, 10, 8, 2, 22, 55, 372, DateTimeKind.Unspecified).AddTicks(5820), new DateTime(2022, 5, 8, 3, 44, 4, 333, DateTimeKind.Unspecified).AddTicks(8074), "Dolorem nihil itaque voluptatem.", "Deserunt dolores molestiae molestiae facilis nostrum quo.", 14 },
                    { 9, 22, new DateTime(2020, 9, 4, 15, 11, 21, 727, DateTimeKind.Unspecified).AddTicks(7506), new DateTime(2021, 12, 5, 22, 47, 23, 604, DateTimeKind.Unspecified).AddTicks(3354), "est", "Libero non suscipit et consectetur aliquid a.", 8 },
                    { 30, 14, new DateTime(2020, 7, 19, 19, 27, 54, 558, DateTimeKind.Unspecified).AddTicks(5290), new DateTime(2021, 3, 21, 17, 56, 17, 441, DateTimeKind.Unspecified).AddTicks(1420), "natus", "Laboriosam eos aliquam sunt magni corporis eos.", 14 },
                    { 19, 14, new DateTime(2020, 11, 6, 9, 50, 18, 978, DateTimeKind.Unspecified).AddTicks(2306), new DateTime(2021, 6, 12, 10, 10, 2, 970, DateTimeKind.Unspecified).AddTicks(7774), "Consequatur natus error. Commodi saepe ducimus. Accusamus nihil cumque rerum aut aperiam quia autem. Aspernatur sit dolor necessitatibus cum.", "Quis rerum nemo sit debitis non quae.", 13 },
                    { 26, 1, new DateTime(2020, 3, 3, 18, 34, 6, 242, DateTimeKind.Unspecified).AddTicks(1414), new DateTime(2022, 4, 19, 12, 46, 15, 795, DateTimeKind.Unspecified).AddTicks(6780), "Repellendus quaerat autem corrupti accusamus sapiente eum recusandae.", "Suscipit aut doloribus provident quia est dolorum.", 6 },
                    { 17, 61, new DateTime(2020, 6, 15, 21, 28, 40, 324, DateTimeKind.Unspecified).AddTicks(8233), new DateTime(2021, 5, 25, 20, 1, 52, 191, DateTimeKind.Unspecified).AddTicks(6556), "sunt", "Ad at ea dignissimos earum eveniet repudiandae.", 9 },
                    { 22, 6, new DateTime(2020, 10, 8, 4, 8, 36, 977, DateTimeKind.Unspecified).AddTicks(6970), new DateTime(2021, 6, 18, 10, 21, 31, 127, DateTimeKind.Unspecified).AddTicks(1205), "Fugiat odio repellat qui veniam sed eligendi rerum tempora rerum. Reiciendis alias perferendis nesciunt maiores sint eveniet quod. Dolores aliquam consectetur mollitia molestiae qui doloremque cum dolores libero. Repellendus quo officiis excepturi cumque dolorem accusantium expedita aperiam. Earum id molestiae tempora culpa eligendi et possimus perferendis et.", "Natus optio et sint repellat omnis iste.", 12 },
                    { 27, 36, new DateTime(2020, 6, 12, 21, 1, 39, 983, DateTimeKind.Unspecified).AddTicks(2191), new DateTime(2021, 5, 23, 23, 11, 37, 843, DateTimeKind.Unspecified).AddTicks(6665), "Facilis soluta dolores qui.", "Aspernatur sed aut quo fugiat quis aut.", 1 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 12, new DateTime(2021, 3, 8, 20, 25, 24, 781, DateTimeKind.Unspecified).AddTicks(1360), "Quo quidem quibusdam. Libero fugiat et et. Dignissimos expedita id. Earum quis ipsam ea qui inventore voluptatem atque maiores.", null, "Aperiam sit ut quia officia earum non.", 4, 3, 2 },
                    { 46, new DateTime(2021, 4, 16, 5, 39, 29, 577, DateTimeKind.Unspecified).AddTicks(4544), "magnam", null, "Adipisci sit quia ut dolor beatae vel.", 22, 23, 3 },
                    { 27, new DateTime(2021, 3, 11, 1, 43, 11, 153, DateTimeKind.Unspecified).AddTicks(9299), "Nostrum provident ullam. Laudantium voluptates cumque ab eaque aut. Sunt aut ipsam in atque quos eius. Nesciunt laborum et. Est placeat dolorum vitae quasi occaecati est officiis. Aliquam eum sequi non.", null, "Sequi quibusdam repellendus magni quis nostrum eum.", 7, 23, 3 },
                    { 16, new DateTime(2021, 3, 12, 7, 2, 30, 465, DateTimeKind.Unspecified).AddTicks(5478), "Molestiae inventore vitae dolorem. Velit omnis exercitationem expedita omnis voluptas consequatur est. Et ea quidem nemo dicta minus.", null, "Qui ut sunt non non libero dolores.", 26, 23, 3 },
                    { 7, new DateTime(2021, 4, 19, 19, 40, 40, 636, DateTimeKind.Unspecified).AddTicks(6110), "minima", null, "Ut voluptas ab molestiae deleniti iste non.", 67, 23, 0 },
                    { 71, new DateTime(2021, 1, 8, 18, 14, 50, 382, DateTimeKind.Unspecified).AddTicks(7897), "Est a tempora quam tempore.", null, "Cupiditate ratione nostrum ut impedit magni quos.", 73, 20, 1 },
                    { 112, new DateTime(2021, 2, 6, 18, 0, 2, 398, DateTimeKind.Unspecified).AddTicks(7504), "Distinctio iure tempora provident et error. Ea sint tempora sit. Repellendus odit ut sequi quia libero delectus cum harum non. Nostrum qui ducimus molestiae neque delectus. Dolor at dolores accusamus recusandae.", null, "Qui mollitia voluptatem voluptas nihil fugiat esse.", 23, 4, 0 },
                    { 88, new DateTime(2021, 1, 4, 13, 13, 1, 977, DateTimeKind.Unspecified).AddTicks(6025), "Sit excepturi et dolorum tempora sit.", null, "Cupiditate omnis molestiae similique molestiae aspernatur eaque.", 3, 4, 2 },
                    { 87, new DateTime(2021, 3, 3, 11, 19, 50, 765, DateTimeKind.Unspecified).AddTicks(7529), "Inventore qui itaque dolores consequuntur.\nUt et tempore doloremque consequatur error autem sunt numquam quia.\nVoluptatem iste accusamus ullam ipsam et necessitatibus dolorum.\nNostrum voluptatem sint sed voluptatem voluptatem eos veritatis.", null, "Saepe assumenda et qui labore voluptas corrupti.", 33, 4, 1 },
                    { 45, new DateTime(2021, 1, 8, 15, 41, 26, 555, DateTimeKind.Unspecified).AddTicks(8215), "Odit non corrupti et rem saepe cumque non consequatur aperiam.", new DateTime(2021, 6, 3, 11, 59, 41, 69, DateTimeKind.Unspecified).AddTicks(9484), "Ratione dolor fugiat consequatur explicabo reiciendis architecto.", 34, 4, 3 },
                    { 72, new DateTime(2021, 2, 22, 18, 21, 57, 47, DateTimeKind.Unspecified).AddTicks(3435), "harum", null, "Hic eius provident delectus aliquam est vel.", 12, 2, 0 },
                    { 38, new DateTime(2021, 3, 29, 2, 30, 38, 220, DateTimeKind.Unspecified).AddTicks(9267), "In dignissimos reprehenderit aut qui et qui iusto.\nNeque dolorem ipsum occaecati quisquam cupiditate explicabo doloribus necessitatibus.\nUt soluta doloremque porro sint ipsa.\nQuo velit qui dolores quis.", null, "Veritatis rerum quo est voluptatem dolore animi.", 43, 2, 0 },
                    { 20, new DateTime(2021, 4, 27, 17, 15, 32, 978, DateTimeKind.Unspecified).AddTicks(5069), "aut", null, "A temporibus unde debitis dolores praesentium autem.", 53, 2, 3 },
                    { 89, new DateTime(2021, 4, 20, 15, 32, 56, 552, DateTimeKind.Unspecified).AddTicks(7940), "Ipsam consectetur nihil et cum id.\nRerum minus iure non at nostrum exercitationem a consequatur.", null, "Ab debitis odio qui est quo eos.", 42, 23, 3 },
                    { 18, new DateTime(2021, 2, 18, 6, 35, 35, 296, DateTimeKind.Unspecified).AddTicks(4361), "Beatae est non doloremque eum id commodi.\nVoluptas suscipit autem eos qui.\nPerspiciatis commodi tempora nesciunt id perferendis.\nDolorum quod omnis autem aut nihil animi eligendi.\nExcepturi sequi provident corporis non molestias.", new DateTime(2021, 6, 17, 22, 48, 20, 421, DateTimeKind.Unspecified).AddTicks(6098), "Nulla exercitationem distinctio dolores velit amet numquam.", 52, 2, 1 },
                    { 31, new DateTime(2021, 2, 6, 16, 36, 43, 314, DateTimeKind.Unspecified).AddTicks(2158), "Qui dolores velit voluptatem vitae.\nSapiente est aut hic mollitia rerum qui.", null, "Nihil inventore eius non nostrum vel fuga.", 69, 25, 2 },
                    { 23, new DateTime(2021, 3, 9, 14, 33, 55, 808, DateTimeKind.Unspecified).AddTicks(9317), "Totam voluptas quae rerum est accusamus dolorem.\nDolores fugiat reprehenderit et.\nOmnis tempora ut ad et molestiae ullam.\nSed eveniet adipisci at sit aut doloribus.\nDolorum qui expedita veniam esse blanditiis et deserunt ut.", null, "Et quam dolorem eum illo suscipit et.", 47, 25, 1 },
                    { 10, new DateTime(2021, 4, 18, 6, 31, 31, 611, DateTimeKind.Unspecified).AddTicks(8596), "Qui ab unde repudiandae nostrum cum. Consectetur quod omnis. In et beatae enim quae voluptatem aperiam et non eum.", null, "Consequatur et nam aliquam quaerat rerum qui.", 21, 25, 2 },
                    { 4, new DateTime(2021, 4, 13, 12, 57, 56, 680, DateTimeKind.Unspecified).AddTicks(381), "Odit aspernatur reiciendis odio et.\nNemo quia vel deserunt quo possimus nihil numquam.\nTotam voluptate et dolores eum et dolores quia laborum.", null, "Quos pariatur vero eius vel quisquam ipsum.", 77, 25, 3 },
                    { 109, new DateTime(2021, 3, 29, 23, 31, 31, 169, DateTimeKind.Unspecified).AddTicks(6991), "Aspernatur dolores inventore temporibus illo.\nSequi expedita et maxime debitis id impedit dolorem quod.\nSuscipit et exercitationem debitis consequuntur magni et.\nNumquam quia maxime sapiente dolorem aut error.\nMaiores at laudantium ratione voluptates et vel optio.\nTempore autem eligendi quam recusandae vel voluptatem cumque voluptas.", null, "Libero occaecati reiciendis dignissimos ex voluptatem rerum.", 24, 14, 2 },
                    { 108, new DateTime(2021, 2, 5, 12, 20, 42, 610, DateTimeKind.Unspecified).AddTicks(8432), "Suscipit placeat quo occaecati tempora necessitatibus tempore repellendus quas natus. Quidem hic sed est. Iure accusantium atque molestiae laboriosam omnis dolor blanditiis nihil. Eum rerum qui. Voluptate occaecati dolores sed et tenetur aut natus. Ut officia iusto deleniti et.", null, "Recusandae iure quam ex laboriosam commodi totam.", 41, 14, 1 },
                    { 78, new DateTime(2021, 1, 27, 5, 32, 38, 376, DateTimeKind.Unspecified).AddTicks(8280), "Fuga facilis excepturi pariatur rerum esse officia minus.", null, "Dicta reiciendis ut alias incidunt rem enim.", 27, 14, 3 },
                    { 104, new DateTime(2021, 1, 18, 16, 16, 59, 239, DateTimeKind.Unspecified).AddTicks(847), "Voluptas vero excepturi quibusdam quas beatae consequatur qui quae nobis.", null, "Quis quia doloribus voluptas qui perspiciatis esse.", 22, 5, 3 },
                    { 82, new DateTime(2021, 2, 7, 5, 0, 24, 827, DateTimeKind.Unspecified).AddTicks(1861), "Consequuntur occaecati at.\nAspernatur tenetur tenetur quibusdam voluptatem at ratione.\nDolor a sint iusto cum natus maxime voluptas repellendus.\nProvident iusto adipisci animi amet aut necessitatibus culpa quis explicabo.\nAspernatur voluptatem cupiditate.\nDoloremque tenetur voluptas omnis voluptas.", null, "Iure dolores est dicta et nisi tenetur.", 15, 5, 3 },
                    { 59, new DateTime(2021, 2, 6, 15, 30, 38, 256, DateTimeKind.Unspecified).AddTicks(6621), "Exercitationem placeat assumenda saepe consequatur. Magni aut voluptas aliquid incidunt delectus sunt quia consequatur qui. Tempore maiores animi sit debitis magnam vero consectetur. Rerum quas aperiam expedita beatae. Nemo officia sapiente illum sunt.", null, "Impedit molestiae quaerat explicabo quis qui nihil.", 56, 5, 0 },
                    { 58, new DateTime(2021, 2, 13, 6, 17, 32, 923, DateTimeKind.Unspecified).AddTicks(5094), "Mollitia voluptatem voluptatum et. Sapiente recusandae aliquam error nisi. Fugit id accusamus.", null, "Repellendus quisquam rem aut labore iure omnis.", 63, 5, 3 },
                    { 110, new DateTime(2021, 1, 22, 14, 46, 40, 920, DateTimeKind.Unspecified).AddTicks(9358), "corrupti", null, "Magni ut atque cupiditate sunt nulla ipsa.", 8, 8, 1 },
                    { 60, new DateTime(2021, 2, 23, 8, 29, 29, 618, DateTimeKind.Unspecified).AddTicks(8546), "Facere ut illo sit.\nEt repudiandae nihil sed ab fuga.\nLibero ad necessitatibus expedita maiores nesciunt unde magnam praesentium consequatur.\nEos quia nulla totam.", null, "Saepe aperiam repellendus laudantium maxime aliquid dolores.", 60, 25, 1 },
                    { 106, new DateTime(2021, 1, 26, 16, 19, 21, 739, DateTimeKind.Unspecified).AddTicks(3781), "Eveniet quas ipsam vitae rerum vero vel dicta consequuntur molestiae.\nLaboriosam quis et laboriosam voluptatem alias aut repellat.\nDoloribus quae sint debitis perspiciatis.\nQuisquam cum optio quos.", null, "Cupiditate excepturi ipsum iusto ratione tempore necessitatibus.", 61, 8, 3 },
                    { 115, new DateTime(2021, 2, 7, 4, 24, 16, 959, DateTimeKind.Unspecified).AddTicks(6437), "Deleniti nesciunt non consequatur dicta nihil voluptate id repellendus repellat.", null, "Esse ea quod ut consequatur est deleniti.", 36, 23, 1 },
                    { 33, new DateTime(2021, 1, 3, 5, 39, 57, 536, DateTimeKind.Unspecified).AddTicks(3050), "Dignissimos dolor repellendus voluptatem rerum et et sed debitis omnis.", null, "Earum laboriosam quia omnis consequatur iusto repellat.", 77, 11, 3 },
                    { 77, new DateTime(2021, 4, 9, 2, 3, 15, 749, DateTimeKind.Unspecified).AddTicks(9716), "Dolor quod quibusdam sit fugit fugiat enim est ex.", null, "Provident et voluptatem cupiditate eum enim ut.", 57, 22, 1 },
                    { 69, new DateTime(2021, 3, 10, 12, 55, 57, 255, DateTimeKind.Unspecified).AddTicks(1376), "Quas ratione aut optio nihil velit sit placeat laboriosam.", null, "Dolorem et est nam et reprehenderit ea.", 62, 22, 2 },
                    { 56, new DateTime(2021, 3, 18, 9, 8, 57, 73, DateTimeKind.Unspecified).AddTicks(2847), "harum", null, "Ut sed consequatur animi nesciunt dolorem doloribus.", 54, 22, 1 },
                    { 17, new DateTime(2021, 4, 15, 14, 10, 30, 195, DateTimeKind.Unspecified).AddTicks(9959), "quibusdam", null, "Corporis molestiae aut incidunt recusandae et reprehenderit.", 57, 22, 0 },
                    { 6, new DateTime(2021, 4, 23, 8, 33, 50, 399, DateTimeKind.Unspecified).AddTicks(8583), "Quo tempore aperiam dicta. Eos hic nam est sint eos voluptates. Quia quasi doloremque dolores consequatur ab accusamus officia itaque. Nisi est voluptatem sit facere laudantium officiis fugit molestiae alias.", null, "Consectetur ab vitae harum atque aut vero.", 5, 22, 1 },
                    { 117, new DateTime(2021, 1, 29, 15, 23, 44, 660, DateTimeKind.Unspecified).AddTicks(6755), "deleniti", null, "Officiis asperiores et ullam rerum aut ipsa.", 30, 1, 3 },
                    { 116, new DateTime(2021, 4, 10, 17, 50, 34, 804, DateTimeKind.Unspecified).AddTicks(1221), "exercitationem", null, "Ducimus at facere quibusdam fuga pariatur neque.", 33, 1, 2 },
                    { 99, new DateTime(2021, 3, 29, 15, 14, 39, 382, DateTimeKind.Unspecified).AddTicks(799), "Sit doloribus omnis omnis qui aut illo accusamus.\nVoluptatibus magni molestiae ea occaecati in vel non ipsum.\nRepellat modi quis eius praesentium dignissimos quis facilis.\nQui explicabo qui quod consectetur id aspernatur.\nTotam quaerat molestiae ab ut.", new DateTime(2021, 6, 2, 3, 23, 25, 30, DateTimeKind.Unspecified).AddTicks(6271), "Placeat enim adipisci nesciunt sed harum id.", 56, 1, 3 },
                    { 95, new DateTime(2021, 2, 26, 16, 33, 39, 773, DateTimeKind.Unspecified).AddTicks(668), "Distinctio vero officia occaecati.\nExercitationem sit mollitia accusamus.\nAccusamus nihil laboriosam dolorem libero.\nEos nulla aspernatur laboriosam tempore et odit quidem qui qui.", null, "Et aliquam repudiandae omnis dolorem est enim.", 7, 1, 2 },
                    { 83, new DateTime(2021, 4, 29, 18, 1, 41, 373, DateTimeKind.Unspecified).AddTicks(3092), "voluptatem", null, "Ipsam est sunt dolor libero reprehenderit a.", 51, 1, 0 },
                    { 74, new DateTime(2021, 2, 26, 18, 19, 11, 803, DateTimeKind.Unspecified).AddTicks(9025), "Necessitatibus animi nesciunt unde dolores laboriosam quo.", null, "Aliquam est officiis qui amet fugit dignissimos.", 76, 1, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 66, new DateTime(2021, 1, 13, 20, 10, 37, 636, DateTimeKind.Unspecified).AddTicks(4002), "Nam distinctio natus veritatis aut voluptas et omnis explicabo provident.", null, "Aliquid ab praesentium aut ea pariatur facilis.", 29, 1, 1 },
                    { 26, new DateTime(2021, 1, 22, 7, 46, 0, 995, DateTimeKind.Unspecified).AddTicks(626), "Nostrum soluta possimus vero et omnis repudiandae aut. Exercitationem reiciendis nihil aut ut quam. Sed consequatur eveniet quaerat adipisci velit debitis dignissimos praesentium. Veritatis eligendi at enim.", null, "A odit reprehenderit rerum omnis et eos.", 22, 11, 2 },
                    { 62, new DateTime(2021, 3, 18, 18, 24, 59, 480, DateTimeKind.Unspecified).AddTicks(1501), "Est incidunt officia hic mollitia molestiae maxime dolorem.\nA illum ex omnis est animi.\nEum itaque qui veritatis ab.\nQuis eos magnam sequi blanditiis ut qui sed natus illo.", null, "Quaerat et ullam pariatur consequatur nesciunt hic.", 42, 1, 2 },
                    { 25, new DateTime(2021, 2, 15, 6, 16, 58, 320, DateTimeKind.Unspecified).AddTicks(8405), "Placeat quod iste architecto. Eius impedit esse ad qui in harum. Perspiciatis aut dolores dolor. Deleniti ut doloribus earum nulla ullam.", null, "Excepturi est quo sunt natus officia ut.", 40, 1, 3 },
                    { 102, new DateTime(2021, 1, 22, 22, 53, 31, 374, DateTimeKind.Unspecified).AddTicks(4596), "In in voluptate laudantium quis dolore harum distinctio impedit minima.", null, "Aperiam consectetur ea culpa eius voluptatem officia.", 4, 12, 2 },
                    { 91, new DateTime(2021, 1, 28, 1, 58, 37, 12, DateTimeKind.Unspecified).AddTicks(2759), "Ullam aliquid maiores adipisci officia qui et voluptas.", null, "Ut beatae qui aut repellat rerum corrupti.", 75, 12, 3 },
                    { 63, new DateTime(2021, 1, 8, 11, 42, 30, 763, DateTimeKind.Unspecified).AddTicks(9290), "nihil", null, "Fuga minus temporibus ea ducimus eaque vitae.", 76, 12, 3 },
                    { 37, new DateTime(2021, 4, 24, 2, 12, 58, 289, DateTimeKind.Unspecified).AddTicks(3589), "Dolor ut est debitis id aut omnis ducimus odit. Cum molestiae qui vel. Ut eius saepe dolore harum cumque amet aut qui facilis.", null, "Maxime culpa numquam ab et reprehenderit esse.", 44, 12, 3 },
                    { 24, new DateTime(2021, 4, 22, 10, 32, 48, 828, DateTimeKind.Unspecified).AddTicks(5332), "Enim eum consequatur voluptatem accusamus pariatur temporibus. Maiores dolor ex sint eligendi id. Amet explicabo laborum cum non omnis et ut totam.", null, "Non ut consectetur commodi illum enim sequi.", 6, 12, 1 },
                    { 96, new DateTime(2021, 1, 19, 0, 33, 34, 910, DateTimeKind.Unspecified).AddTicks(9193), "et", null, "Quaerat repellat amet sit reiciendis voluptatem qui.", 68, 28, 1 },
                    { 85, new DateTime(2021, 3, 11, 19, 45, 26, 574, DateTimeKind.Unspecified).AddTicks(8309), "Ea itaque exercitationem eum aut ut ut.", null, "Eos ut dolor consequuntur sed voluptates animi.", 79, 28, 0 },
                    { 61, new DateTime(2021, 3, 10, 5, 3, 9, 80, DateTimeKind.Unspecified).AddTicks(6907), "Excepturi assumenda facere fugit tenetur commodi molestias in.\nConsequatur reiciendis quia.\nNobis iure repellendus adipisci.", null, "Omnis similique excepturi quia quis aliquam aliquam.", 60, 28, 0 },
                    { 118, new DateTime(2021, 1, 23, 21, 42, 55, 713, DateTimeKind.Unspecified).AddTicks(3846), "Et aut at. Qui quam et laudantium reiciendis inventore. Provident quod ipsum. Quia velit sapiente ea delectus voluptates sint iusto.", null, "Expedita quo beatae tenetur voluptatum dolores voluptate.", 26, 11, 1 },
                    { 80, new DateTime(2021, 3, 9, 4, 59, 46, 93, DateTimeKind.Unspecified).AddTicks(5398), "Ullam numquam totam quia adipisci placeat nulla.\nConsectetur dolor rerum mollitia eum.", null, "Maiores aut aut facilis incidunt cum error.", 10, 11, 1 },
                    { 41, new DateTime(2021, 2, 12, 4, 16, 49, 313, DateTimeKind.Unspecified).AddTicks(1076), "Voluptatem et quisquam repellat mollitia aut culpa. Totam magni deleniti praesentium quo laudantium sit et sed accusamus. Accusantium molestiae ducimus beatae et eos. Similique nobis odio nulla nulla dignissimos. Repudiandae eligendi maiores nemo dolorum voluptatem qui exercitationem asperiores reiciendis. Qui debitis rerum modi.", null, "Laboriosam repellendus nobis accusantium pariatur velit adipisci.", 70, 11, 0 },
                    { 57, new DateTime(2021, 4, 22, 23, 43, 21, 898, DateTimeKind.Unspecified).AddTicks(9765), "Consequatur architecto qui exercitationem aut at cupiditate molestiae molestias.", null, "Id non cum quia incidunt provident dolores.", 37, 1, 1 },
                    { 51, new DateTime(2021, 4, 3, 22, 58, 26, 469, DateTimeKind.Unspecified).AddTicks(3387), "Vel minima maiores qui vel in dolorem. Corporis sequi ut maiores et. Praesentium et sit odio consequuntur. Libero nihil voluptatem nihil accusantium eos fugiat error soluta vel. Voluptatem quia et est. Impedit et vitae qui asperiores.", null, "Dolores quo ut magnam vitae dolorum et.", 22, 8, 1 },
                    { 103, new DateTime(2021, 4, 2, 16, 16, 52, 550, DateTimeKind.Unspecified).AddTicks(8088), "Expedita ut ex a aliquid repellat accusamus illum.", null, "Eveniet et pariatur officia perspiciatis quisquam quae.", 32, 16, 0 },
                    { 64, new DateTime(2021, 2, 16, 4, 12, 55, 140, DateTimeKind.Unspecified).AddTicks(6500), "Dignissimos aut deleniti culpa minima qui omnis libero. Ea expedita minus explicabo voluptatem modi et aut eveniet non. Nisi sunt molestiae sit sunt officiis. Sed aliquid sapiente quod repellat ipsum aperiam.", null, "Perferendis voluptas perferendis itaque laboriosam soluta et.", 20, 16, 1 },
                    { 105, new DateTime(2021, 1, 13, 14, 1, 19, 7, DateTimeKind.Unspecified).AddTicks(320), "Consequuntur laborum autem eos dolorem.\nDolor et et id.\nFugit nulla neque pariatur sint et sunt delectus vitae numquam.\nAnimi autem dolores.", null, "Quos recusandae nostrum minima nam voluptas praesentium.", 60, 21, 3 },
                    { 47, new DateTime(2021, 1, 23, 18, 46, 56, 446, DateTimeKind.Unspecified).AddTicks(5741), "ratione", null, "Temporibus sed quas vero qui possimus consequuntur.", 12, 21, 2 },
                    { 21, new DateTime(2021, 4, 30, 9, 48, 6, 98, DateTimeKind.Unspecified).AddTicks(5248), "Sint voluptatem distinctio pariatur totam voluptates nam consequatur odit ducimus.\nCulpa quis laudantium reiciendis.", null, "Minima voluptatem placeat qui consequatur consequatur necessitatibus.", 35, 9, 1 },
                    { 19, new DateTime(2021, 4, 19, 19, 1, 57, 636, DateTimeKind.Unspecified).AddTicks(5365), "Illo illum possimus delectus praesentium maiores assumenda labore illum ipsam.\nOfficia dignissimos qui modi sit sint rem eaque a.\nMollitia dignissimos qui et deserunt aut dolores consequatur sed.\nPerferendis vero iusto blanditiis.\nEst laboriosam rem occaecati quidem.\nMaxime reiciendis tempore sed.", null, "Temporibus doloremque qui ut voluptatibus rem quaerat.", 35, 9, 2 },
                    { 113, new DateTime(2021, 1, 3, 7, 35, 22, 312, DateTimeKind.Unspecified).AddTicks(2663), "Enim fuga placeat. Quae eos dolores aut itaque vitae quia quidem voluptatem odio. Dignissimos voluptas dolorem repudiandae.", new DateTime(2021, 6, 26, 10, 59, 56, 820, DateTimeKind.Unspecified).AddTicks(9911), "Natus enim non dignissimos rerum aspernatur iusto.", 76, 30, 0 },
                    { 107, new DateTime(2021, 4, 29, 10, 51, 14, 804, DateTimeKind.Unspecified).AddTicks(8724), "Alias sit eos ut aspernatur repellat libero debitis nihil.\nSint at officia iusto.\nQuisquam dolores cumque iure quidem totam sed.\nConsectetur natus omnis dolore a.\nDeleniti dolore et et vitae.\nEa dicta sit culpa cumque qui vitae molestias vero voluptatem.", null, "Quasi in ut ipsa et occaecati sed.", 15, 30, 0 },
                    { 68, new DateTime(2021, 1, 30, 23, 31, 51, 222, DateTimeKind.Unspecified).AddTicks(1351), "Quisquam ipsum inventore officiis quasi sed quidem sed cupiditate dolore.", new DateTime(2021, 6, 1, 21, 53, 12, 638, DateTimeKind.Unspecified).AddTicks(3831), "Quas ducimus qui eum exercitationem et adipisci.", 27, 30, 1 },
                    { 30, new DateTime(2021, 2, 24, 16, 47, 41, 487, DateTimeKind.Unspecified).AddTicks(8338), "Aut impedit tempora rem reprehenderit nam inventore exercitationem.\nEum quia earum aspernatur impedit esse ut incidunt sit.\nQuaerat consectetur rerum ipsum rerum.", null, "Officia voluptatibus distinctio aut voluptatem nobis fuga.", 7, 30, 0 },
                    { 92, new DateTime(2021, 4, 25, 23, 47, 7, 181, DateTimeKind.Unspecified).AddTicks(1584), "Animi eos sint iure eius illum alias at delectus.\nEligendi omnis id quis aut eos error.\nPerspiciatis iusto repellat error assumenda voluptates tenetur quibusdam officia aspernatur.\nNihil fugiat omnis ipsa nihil placeat nulla minima sed molestiae.\nQuo alias quaerat aut sit amet perspiciatis voluptatem optio.", null, "Hic minima omnis impedit aut cupiditate et.", 2, 19, 0 },
                    { 81, new DateTime(2021, 1, 29, 11, 23, 49, 54, DateTimeKind.Unspecified).AddTicks(9855), "Necessitatibus quas molestiae voluptas quibusdam voluptas sed ducimus quae.\nEt ab veniam molestias.", null, "Laborum doloremque enim nihil quia eligendi laborum.", 27, 19, 2 },
                    { 43, new DateTime(2021, 2, 27, 20, 11, 6, 746, DateTimeKind.Unspecified).AddTicks(793), "Et totam vero pariatur voluptatibus quos nisi cum. Quidem sint molestiae. Dolor doloribus adipisci. Atque et molestias in quos ea non ea sit. Qui reiciendis nesciunt.", null, "Aperiam perspiciatis molestiae est doloremque adipisci culpa.", 23, 19, 2 },
                    { 9, new DateTime(2021, 4, 4, 22, 25, 40, 575, DateTimeKind.Unspecified).AddTicks(724), "incidunt", null, "Molestias aut neque alias ratione debitis consequatur.", 1, 19, 1 },
                    { 5, new DateTime(2021, 1, 25, 19, 15, 34, 582, DateTimeKind.Unspecified).AddTicks(7394), "Dolorem et ut blanditiis minima. Dolore minus qui facilis. Repudiandae ipsam ut iure rerum pariatur. Saepe consequatur perspiciatis est et accusamus quia repudiandae amet autem.", null, "Alias ut ea laboriosam laborum et corrupti.", 35, 10, 0 },
                    { 76, new DateTime(2021, 1, 29, 7, 3, 4, 809, DateTimeKind.Unspecified).AddTicks(1153), "Reprehenderit optio quod a quibusdam laborum nihil eligendi odio.", null, "Numquam vel quas similique fugit sed magnam.", 30, 26, 0 },
                    { 29, new DateTime(2021, 4, 20, 21, 4, 7, 706, DateTimeKind.Unspecified).AddTicks(3367), "rem", null, "Et provident et ut nemo fugiat aut.", 66, 26, 2 },
                    { 13, new DateTime(2021, 4, 27, 1, 13, 21, 173, DateTimeKind.Unspecified).AddTicks(3342), "dolorum", null, "Aut illo voluptatem asperiores sed neque possimus.", 44, 26, 2 },
                    { 100, new DateTime(2021, 1, 17, 15, 30, 1, 738, DateTimeKind.Unspecified).AddTicks(6546), "Assumenda ullam aut beatae consequatur odio rem cum qui iure.\nQuibusdam quam quia ut.\nEt qui doloremque corporis rerum sit eveniet quia.\nUt aperiam aut reprehenderit voluptatem possimus in.", null, "Accusamus tempore culpa iure nihil iusto maiores.", 26, 17, 0 },
                    { 73, new DateTime(2021, 4, 10, 9, 10, 58, 929, DateTimeKind.Unspecified).AddTicks(7551), "Ipsa non quia.", null, "Commodi molestiae quia hic qui nisi sed.", 25, 17, 2 },
                    { 35, new DateTime(2021, 3, 6, 5, 37, 36, 203, DateTimeKind.Unspecified).AddTicks(9796), "et", null, "Voluptatum consequatur aliquam aut iure nisi deserunt.", 48, 17, 3 },
                    { 34, new DateTime(2021, 1, 6, 1, 44, 22, 51, DateTimeKind.Unspecified).AddTicks(4733), "Quis impedit ut odit magni.", null, "Sit odio velit voluptatibus quidem qui perferendis.", 67, 17, 3 },
                    { 8, new DateTime(2021, 1, 4, 22, 20, 9, 666, DateTimeKind.Unspecified).AddTicks(7886), "Tenetur adipisci sed est in.\nVoluptates iusto labore enim adipisci eveniet ipsa alias dignissimos aut.\nNihil qui ducimus doloremque ad consectetur.", null, "Voluptas quos sint quos sunt est possimus.", 37, 17, 0 },
                    { 90, new DateTime(2021, 4, 5, 15, 21, 49, 465, DateTimeKind.Unspecified).AddTicks(8869), "ad", null, "Voluptatum aperiam rerum repellat inventore sequi dicta.", 7, 3, 3 },
                    { 86, new DateTime(2021, 3, 14, 3, 56, 2, 812, DateTimeKind.Unspecified).AddTicks(9278), "Repudiandae adipisci voluptas et. Omnis ullam sint blanditiis adipisci ab sequi eos consequatur consequatur. Reprehenderit iusto expedita quae non et quidem saepe.", null, "Dolor sed voluptas autem incidunt assumenda consequatur.", 2, 3, 0 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 52, new DateTime(2021, 4, 18, 5, 9, 8, 140, DateTimeKind.Unspecified).AddTicks(7832), "Odit a minima quibusdam hic similique animi culpa corrupti maiores. Distinctio consequatur est. Sunt qui ex corporis. Ea non modi optio excepturi necessitatibus velit omnis in non. Reprehenderit dignissimos animi et quis et quidem. Aliquam reprehenderit soluta quis.", null, "Excepturi ut quidem temporibus minima id et.", 1, 3, 0 },
                    { 48, new DateTime(2021, 2, 10, 8, 59, 17, 745, DateTimeKind.Unspecified).AddTicks(5878), "Est sed voluptatem et qui assumenda ut quod mollitia.\nIste eos totam provident.", null, "Atque reiciendis id fugit ea veritatis voluptatem.", 42, 3, 0 },
                    { 32, new DateTime(2021, 3, 29, 10, 31, 20, 232, DateTimeKind.Unspecified).AddTicks(2726), "Repudiandae est quis dicta. Amet accusamus iusto vel pariatur officiis aut error molestiae unde. Aut quaerat sed id laborum et non. Cumque doloribus quod dicta est. Non dignissimos corporis dignissimos ab repellat mollitia eos. Cumque qui quia quos incidunt et sed aut.", null, "Et sed laboriosam ad recusandae fugiat sed.", 43, 3, 1 },
                    { 53, new DateTime(2021, 4, 10, 4, 43, 57, 416, DateTimeKind.Unspecified).AddTicks(1664), "Id culpa veniam veritatis.", null, "Voluptates distinctio consectetur sit enim blanditiis recusandae.", 49, 26, 1 },
                    { 49, new DateTime(2021, 3, 29, 10, 41, 18, 862, DateTimeKind.Unspecified).AddTicks(7012), "Eos unde vel est in dolorum.", null, "Ut ab eum sit laudantium distinctio mollitia.", 51, 10, 3 },
                    { 50, new DateTime(2021, 1, 10, 4, 55, 48, 143, DateTimeKind.Unspecified).AddTicks(7541), "Sed reiciendis voluptatem id reprehenderit ullam quia. Possimus quasi quos enim sit ut explicabo consectetur dolor in. Occaecati optio aut vel omnis iusto ut aperiam eos et. Laborum eligendi magnam saepe eius nesciunt.", null, "Necessitatibus qui incidunt voluptates omnis mollitia at.", 46, 10, 2 },
                    { 55, new DateTime(2021, 4, 5, 17, 29, 7, 638, DateTimeKind.Unspecified).AddTicks(997), "Fugiat nobis et provident quasi ipsa ea autem.\nProvident et omnis fuga est vitae quo aut dicta.", null, "Pariatur aliquid et rerum error et aut.", 51, 10, 0 },
                    { 15, new DateTime(2021, 4, 12, 13, 5, 24, 141, DateTimeKind.Unspecified).AddTicks(9846), "voluptates", null, "Saepe et ullam quia est perferendis quae.", 69, 16, 1 },
                    { 70, new DateTime(2021, 1, 8, 18, 15, 8, 82, DateTimeKind.Unspecified).AddTicks(1506), "Minus harum distinctio illo ut provident.\nSed ipsam repellendus dolore consequatur molestiae sit itaque dolores.", null, "Provident iste ipsum autem veritatis et molestiae.", 51, 15, 1 },
                    { 1, new DateTime(2021, 2, 12, 0, 32, 6, 663, DateTimeKind.Unspecified).AddTicks(2386), "Perspiciatis rerum facere neque voluptatum eligendi. Dicta ipsum nam voluptate. Est molestias adipisci voluptatibus.", null, "Assumenda hic soluta soluta quas eos necessitatibus.", 3, 15, 0 },
                    { 67, new DateTime(2021, 3, 23, 8, 8, 19, 409, DateTimeKind.Unspecified).AddTicks(468), "ut", null, "Et nostrum facilis nobis ea et nostrum.", 64, 29, 0 },
                    { 65, new DateTime(2021, 3, 30, 3, 28, 14, 839, DateTimeKind.Unspecified).AddTicks(1941), "Eos quo quisquam libero.", null, "Et cum earum labore ratione consequuntur officia.", 37, 29, 2 },
                    { 44, new DateTime(2021, 4, 3, 12, 24, 8, 232, DateTimeKind.Unspecified).AddTicks(7817), "velit", null, "Optio ut non harum sequi distinctio ut.", 5, 29, 3 },
                    { 22, new DateTime(2021, 1, 8, 21, 38, 23, 385, DateTimeKind.Unspecified).AddTicks(9540), "Ut sequi explicabo omnis temporibus aperiam natus. Laborum et voluptatem iusto eos et. Enim cumque omnis veniam placeat impedit quis molestiae a. In delectus a et eum rerum maiores consectetur et error.", null, "Ut animi unde commodi id laudantium dolorem.", 48, 29, 1 },
                    { 101, new DateTime(2021, 1, 31, 16, 33, 21, 960, DateTimeKind.Unspecified).AddTicks(3290), "beatae", null, "Ratione nulla nobis provident qui illo rerum.", 14, 18, 0 },
                    { 75, new DateTime(2021, 3, 27, 18, 5, 36, 27, DateTimeKind.Unspecified).AddTicks(757), "alias", null, "Et quisquam dolor quisquam molestiae et nulla.", 25, 18, 3 },
                    { 3, new DateTime(2021, 1, 11, 21, 24, 48, 982, DateTimeKind.Unspecified).AddTicks(7668), "Eaque sunt iure cumque.\nExpedita eum dolorum in.\nRerum dolor mollitia at quaerat quibusdam quia magnam quia.", null, "Corrupti consequatur libero est sed incidunt iure.", 57, 18, 3 },
                    { 2, new DateTime(2021, 1, 20, 23, 8, 18, 670, DateTimeKind.Unspecified).AddTicks(1502), "Aut culpa eaque.\nPerspiciatis corrupti unde reiciendis est accusamus ad non doloribus.\nFuga non dolor ipsum corrupti cupiditate repellat ut consectetur.\nLibero necessitatibus odit molestiae ea.\nMolestiae dolores suscipit dolores omnis eum sed molestiae delectus nihil.", null, "Quod expedita provident eum omnis error voluptas.", 62, 18, 1 },
                    { 94, new DateTime(2021, 3, 2, 4, 15, 31, 524, DateTimeKind.Unspecified).AddTicks(5132), "Corporis possimus culpa voluptas magnam quia ea quo totam quia.\nAut unde quo voluptatum enim assumenda molestias in.\nDolores porro labore qui totam ut qui provident similique mollitia.\nNisi rerum aut minus laborum molestiae libero dolorem.", null, "Quisquam enim sed neque culpa commodi perferendis.", 75, 24, 3 },
                    { 14, new DateTime(2021, 4, 14, 22, 35, 39, 970, DateTimeKind.Unspecified).AddTicks(3329), "Rerum ad culpa.", new DateTime(2021, 6, 4, 0, 14, 34, 747, DateTimeKind.Unspecified).AddTicks(5194), "Consequuntur est aliquid nesciunt facilis eum et.", 74, 24, 3 },
                    { 120, new DateTime(2021, 1, 5, 14, 6, 54, 505, DateTimeKind.Unspecified).AddTicks(3532), "Itaque dignissimos rerum voluptas. Aspernatur sed omnis voluptatem. Iusto cupiditate adipisci veniam odio est. Quibusdam dolorem vel ut ut quod qui ut et aliquam.", null, "Et deleniti quaerat corporis voluptatem quia explicabo.", 51, 7, 0 },
                    { 114, new DateTime(2021, 4, 25, 9, 0, 49, 654, DateTimeKind.Unspecified).AddTicks(2424), "Eligendi quisquam doloremque.\nDicta aut et dolore consequatur repudiandae id molestiae dolore quos.\nOdio assumenda laborum eveniet maiores.\nNihil velit accusantium.\nRerum sequi quae ducimus maxime veritatis sunt.", null, "Ea voluptate nihil dicta non adipisci sit.", 58, 7, 3 },
                    { 42, new DateTime(2021, 3, 2, 6, 43, 15, 148, DateTimeKind.Unspecified).AddTicks(1364), "enim", null, "Sint temporibus magni cumque temporibus deleniti eos.", 27, 7, 3 },
                    { 28, new DateTime(2021, 3, 30, 21, 48, 46, 823, DateTimeKind.Unspecified).AddTicks(8521), "Aut et dolorem ut repellendus iusto corrupti accusamus.", null, "Sunt et qui et in veritatis asperiores.", 76, 7, 2 },
                    { 84, new DateTime(2021, 3, 27, 7, 19, 55, 71, DateTimeKind.Unspecified).AddTicks(2281), "Doloribus odit est est quam numquam earum sapiente.\nVoluptate natus veniam sed.", null, "Et assumenda ratione et soluta deleniti illo.", 35, 13, 0 },
                    { 79, new DateTime(2021, 2, 21, 3, 36, 6, 287, DateTimeKind.Unspecified).AddTicks(93), "Ut magnam sed omnis ipsum iste eum eaque autem quasi. Provident sed voluptatem illum voluptatem adipisci. Veritatis dolorum commodi animi eaque minus similique reiciendis aut. Id id necessitatibus non recusandae minus maxime. Est eos et doloremque aliquid.", null, "Sint repellat neque laudantium dolores accusamus quam.", 48, 13, 3 },
                    { 40, new DateTime(2021, 2, 23, 12, 22, 8, 307, DateTimeKind.Unspecified).AddTicks(1798), "Rem voluptas pariatur doloremque dolorum et cumque officiis.", null, "Velit tempora aliquam impedit nostrum quaerat ipsa.", 10, 13, 3 },
                    { 111, new DateTime(2021, 4, 25, 21, 25, 56, 308, DateTimeKind.Unspecified).AddTicks(5735), "blanditiis", null, "At commodi mollitia id sed cupiditate animi.", 61, 6, 1 },
                    { 98, new DateTime(2021, 3, 2, 4, 10, 2, 271, DateTimeKind.Unspecified).AddTicks(183), "voluptatem", null, "Et ut et ea tempora vel autem.", 29, 6, 3 },
                    { 54, new DateTime(2021, 1, 13, 3, 6, 16, 523, DateTimeKind.Unspecified).AddTicks(7531), "Iste quisquam facilis. Aut distinctio quis. Fugit non consectetur ad itaque.", null, "Odio est numquam assumenda tempora possimus distinctio.", 23, 6, 2 },
                    { 39, new DateTime(2021, 4, 18, 4, 52, 28, 368, DateTimeKind.Unspecified).AddTicks(2936), "ut", null, "Amet enim corporis et non qui atque.", 68, 6, 1 },
                    { 36, new DateTime(2021, 4, 29, 19, 2, 14, 724, DateTimeKind.Unspecified).AddTicks(7561), "Rerum fugit nihil et odio quis voluptas quis modi.", null, "Recusandae iure et sequi fugit mollitia blanditiis.", 31, 6, 3 },
                    { 119, new DateTime(2021, 3, 21, 1, 46, 43, 369, DateTimeKind.Unspecified).AddTicks(6367), "quia", null, "Laboriosam enim optio et consequatur culpa rerum.", 79, 10, 0 },
                    { 97, new DateTime(2021, 3, 29, 4, 38, 44, 716, DateTimeKind.Unspecified).AddTicks(3536), "Ipsum corrupti incidunt facere hic recusandae. Quod perspiciatis eligendi. Perferendis officiis labore. Consectetur facilis quo est.", null, "Possimus quia tempora sint maxime explicabo nulla.", 51, 10, 2 },
                    { 93, new DateTime(2021, 3, 7, 5, 5, 51, 93, DateTimeKind.Unspecified).AddTicks(9339), "ut", null, "Ut debitis eaque quis consequatur iusto quam.", 7, 22, 3 },
                    { 11, new DateTime(2021, 1, 28, 8, 47, 47, 165, DateTimeKind.Unspecified).AddTicks(1852), "Voluptas hic mollitia sapiente porro.", null, "Tempora est itaque placeat dicta quia sunt.", 7, 27, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CuratorProjectID",
                table: "Projects",
                column: "CuratorProjectID");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
