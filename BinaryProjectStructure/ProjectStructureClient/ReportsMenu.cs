﻿using ProjectStructureClient.Services;
using System;
using System.Text;

namespace ProjectStructureClient
{
    public class ReportsMenu
    {
        private readonly OperationService _operationService;

        public ReportsMenu()
        {
            _operationService = new();
            GetMenu();
        }

        private void GetMenu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\n\n\n\n\t\t\t\t\t\tSelect Task\n");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\t\t\t    Name operation:                                  Select operation\n" +
                              "                                                                             and enter number. ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(
                "\t\t\tTask 1 Get all tasks in projects by author                   1\n" +
                "\t\t\tTask 2 Get all tasks on performer                            2\n" +
                "\t\t\tTask 3 Get all tasks that finished at 2021 year              3\n" +
                "\t\t\tTask 4 Get all users oldest than 10 years                    4\n" +
                "\t\t\tTask 5 Sort all users First name and sort task on Name       5\n" +
                "\t\t\tTask 6 Get struct User by Id                                 6\n" +
                "\t\t\tTask 7 Get struct all projects                               7\n" +
                "\n" +
                "\t\t\tEnter:");

            ValidationMenu();
        }

        private void ValidationMenu()
        {
            int selection;

            Console.SetCursorPosition(30, 18);
            while (!int.TryParse(Console.ReadLine(), out selection))
            {
                Error();
            }
            GetOperation(selection);
        }

        private void GetOperation(int selection)
        {
            switch (selection)
            {
                case 1:
                    _operationService.GetTaskOne();
                    ConsoleClearForMenu();
                    break;
                case 2:
                    _operationService.GetTaskSecond();
                    ConsoleClearForMenu();
                    break;
                case 3:
                    _operationService.GetTaskThird();
                    ConsoleClearForMenu();
                    break;
                case 4:
                    _operationService.GetTaskFour();
                    ConsoleClearForMenu();
                    break;
                case 5:
                    _operationService.GetTaskFive();
                    ConsoleClearForMenu();
                    break;
                case 6:
                    _operationService.GetTaskSix();
                    ConsoleClearForMenu();
                    break;
                case 7:
                    _operationService.GetTaskSeven();
                    ConsoleClearForMenu();
                    break;
                default:
                    Console.Clear();
                    Error();
                    break;
            }
        }

        private void ConsoleClearForMenu()
        {
            Console.ReadKey();
            Console.Clear();
            GetMenu();
        }

        private void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.WriteLine("Input error, check if the input is correct!");
            ConsoleClearForMenu();
        }
    }
}
