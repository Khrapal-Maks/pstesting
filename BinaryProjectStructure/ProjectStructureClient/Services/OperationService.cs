﻿using ProjectStructureClient.ModelsDTO;
using System;
using System.Collections.Generic;

namespace ProjectStructureClient.Services
{
    public class OperationService
    {
        private readonly ReportsWebService _webService;

        public OperationService()
        {
            _webService = new();
        }

        public async void GetTaskOne()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 1: Example author id 77");
            int authorId = ValidationId();
            List<Tuple<ProjectDTO, int>> projectsTasks = await _webService.GetReportOne(authorId);

            Console.Clear();
            if (projectsTasks.Count > 0)
            {
                Dictionary<ProjectDTO, int> dict = new Dictionary<ProjectDTO, int>();
                foreach (Tuple<ProjectDTO, int> item in projectsTasks)
                {
                    dict.Add(item.Item1, item.Item2);
                }

                Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", "Project id", "Name project", "Count all tasks in project");
                foreach (KeyValuePair<ProjectDTO, int> item in dict)
                {
                    Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", item.Key.Id, item.Key.Name, item.Value);
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }


            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskSecond()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 2: Example performer id 73");
            int performerId = ValidationId();
            List<TasksDTO> tasks = await _webService.GetReportSecond(performerId);

            Console.Clear();
            if (tasks.Count > 0)
            {
                foreach (TasksDTO task in tasks)
                {
                    Console.WriteLine($"Task id: {task.Id}\ttask name: {task.Name}\tperformer id: {task.PerformerId} etc.");
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }


            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskThird()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 3: Example performer id 20 = true");
            int performerId = ValidationId();
            List<Tuple<int, string>> tasks = await _webService.GetReportThird(performerId);

            Console.Clear();
            if (tasks.Count > 0)
            {
                Console.WriteLine("{0,-12} | {1,-8}", "Task id", "Task name");
                foreach (Tuple<int, string> item in tasks)
                {
                    Console.WriteLine("{0,-12} | {1,-8} ", item.Item1, item.Item2);
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskFour()
        {
            List<Tuple<int, string, List<UserDTO>>> usersTeams = await _webService.GetReportFour();

            Console.Clear();
            if (usersTeams.Count > 0)
            {
                Console.WriteLine("Task 4");
                foreach (Tuple<int, string, List<UserDTO>> item in usersTeams)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"\nTeam id: {item.Item1}\tTeam name: {item.Item2}");
                    foreach (UserDTO user in item.Item3)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine($"Date registration: {user.RegisteredAt}\tUser id: {user.Id}\tUser name: {user.FirstName + " " + user.LastName}");
                    }
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskFive()
        {
            List<Tuple<string, List<TasksDTO>>> allUsersWithTheirTasks = await _webService.GetReportFive();

            Console.Clear();
            if (allUsersWithTheirTasks.Count > 0)
            {
                Console.WriteLine("Task 5");
                foreach (Tuple<string, List<TasksDTO>> user in allUsersWithTheirTasks)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"User name: {user.Item1}");
                    Console.ForegroundColor = ConsoleColor.White;
                    foreach (TasksDTO task in user.Item2)
                    {
                        Console.WriteLine($"Task Id: {task.Id}\ttask name: {task.Name}");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskSix()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\n\t\t\t\t\tTask 6: example user id 73");
            int id = ValidationId();

            Console.Clear();
            UserInfoDTO? res = await _webService.GetReportSix(id);

            if (res == null)
            {
                Console.WriteLine("There is no information on the specified ID.");
            }
            else
            {
                List<UserInfoDTO> userInfos = new();
                userInfos.Add((UserInfoDTO)res);

                if (userInfos.Count > 0)
                {
                    foreach (UserInfoDTO user in userInfos)
                    {
                        Console.WriteLine($"User id: {user.User.Id}\tUser name: {user.User.FirstName + " " + user.User.LastName} etc.");
                        Console.WriteLine($"Project id: {(user.Project == null ? 0 : user.Project.Id)}\tProject name: {(user.Project == null ? null : user.Project.Name)} etc.");
                        Console.WriteLine($"Count all tasks in project: {user.CountTasksInProject}");
                        Console.WriteLine($"Count all tasks user in work: {user.CountTasksInWork}");
                        Console.WriteLine($"The longest task in work: task id: {user.Tasks.Id}\tperformer id: {user.Tasks.PerformerId}\tproject id: {user.Tasks.ProjectId} etc.");
                    }
                }
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        public async void GetTaskSeven()
        {
            List<ProjectInfoDTO> projectInfo = new();
            projectInfo.AddRange(await _webService.GetReportSeven());

            Console.Clear();
            if (projectInfo.Count > 0)
            {
                Console.WriteLine("Tasl 7");
                foreach (ProjectInfoDTO project in projectInfo)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Project id: {project.Project.Id}\tProject name: {project.Project.Name}");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"The longest task description in project: id {project.TaskLongDescription.Id}\ttext: {project.TaskLongDescription.Name}");
                    Console.WriteLine($"The shortest task name in project:       id {project.TasksShortName.Id}\ttext: {project.TasksShortName.Name}");
                    if (project.CountUsersInTeamProject == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    Console.WriteLine($"Count users in team: {project.CountUsersInTeamProject}\n");
                }
            }
            else
            {
                Console.WriteLine("There is no information on the specified ID.");
            }

            Console.WriteLine("\n\nPress enter to back in main menu...");
        }

        private int ValidationId()
        {
            int id;

            Console.WriteLine("\t\t\t\t\tEnter number ID:");
            Console.SetCursorPosition(56, 10);
            while (!int.TryParse(Console.ReadLine(), out id))
            {
                Error();
            }
            return id;
        }

        private static void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Clear();
            Console.WriteLine("\n\n\n\n\n\n\n\n\t\t\t\t\tInput error, check if the input is correct!\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t\t\tEnter number ID:");
            Console.SetCursorPosition(56, 10);
        }
    }
}
