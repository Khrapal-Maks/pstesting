﻿namespace ProjectStructureClient.ModelsDTO
{
    public struct UserInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO Project { get; set; }
        public int CountTasksInProject { get; set; }
        public int CountTasksInWork { get; set; }
        public TasksDTO Tasks { get; set; }
    }
}
