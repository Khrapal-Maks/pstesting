﻿namespace ProjectStructureClient.ModelsDTO
{
    public struct ProjectInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TasksDTO TaskLongDescription { get; set; }
        public TasksDTO TasksShortName { get; set; }
        public int? CountUsersInTeamProject { get; set; }
    }
}
