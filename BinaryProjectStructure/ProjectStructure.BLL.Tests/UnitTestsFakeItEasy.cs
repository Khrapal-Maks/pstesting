﻿using FakeItEasy;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System;
using System.Collections.Generic;
using Xunit;


namespace ProjectStructure.BLL.Tests
{
    public class UnitTestsFakeItEasy
    {
        private readonly NewUserDTO _newUserDTO = new()
        {
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3"
        };

        private readonly UserDTO _userDTO = new()
        {
            Id = 3,
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3",
            RegisteredAt = DateTime.Now,
            TeamId = 2
        };

        private readonly List<UserDTO> _usersDTO = new()
        {
            new UserDTO() { Id = 1, BirthDay = DateTime.Now, Email = "test1@test", FirstName = "user1", LastName = "user1", RegisteredAt = DateTime.Now, TeamId = 1 },
            new UserDTO() { Id = 2, BirthDay = DateTime.Now, Email = "test2@test", FirstName = "user2", LastName = "user2", RegisteredAt = DateTime.Now, TeamId = 1 }
        };

        private readonly IUsersService _usersService;

        public UnitTestsFakeItEasy()
        {
            _usersService = A.Fake<IUsersService>();
        }

        [Fact]
        public async void GetAllUsers_InIUsersService()
        {
            //Arrange            
            A.CallTo(() => _usersService.GetAllUsers()).Returns(_usersDTO);

            //Act
            var actual = await _usersService.GetAllUsers();

            //Assert
            Assert.Equal(_usersDTO, actual);
        }

        [Fact]
        public async void CreateUser_InIUsersService()
        {
            //Arrange   
            A.CallTo(() => _usersService.CreateUser(_newUserDTO)).Returns(_userDTO);

            //Act
            var actual = await _usersService.CreateUser(_newUserDTO);

            //Assert
            Assert.Equal(_userDTO, actual);
        }
    }
}
