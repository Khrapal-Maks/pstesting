﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;
using System;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class TasksServiceTests
    {
        private readonly NewTaskDTO _newTask = new()
        {
           ProjectId = 0,
           Name = "New task",
           Description = "Description task"
        };

        private IUnitOfWork _unitOf;
        private ITasksService _tasksService;
        private IMapper _mapper;

        private readonly MapperConfiguration _mapperConfiguration = new(cfg =>
        {
            cfg.CreateMap<NewTaskDTO, Tasks>();
            cfg.CreateMap<TasksDTO, Tasks>();
            cfg.CreateMap<Tasks, TasksDTO>();
        });

        private readonly DbContextOptions<ProjectContext> _options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase("TestTask")
                .Options;

        [Fact]
        public async void CreateTask_ThanGetTask()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _tasksService = new TasksService(_unitOf, _mapper);

            //Act
            var expected = await _tasksService.CreateTask(_newTask);
            var actual = await _tasksService.GetTask(expected.Id);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void CreateTask_ThanUpdateTask_FinishedAt()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _tasksService = new TasksService(_unitOf, _mapper);

            //Act
            var actual = await _tasksService.CreateTask(_newTask);

            actual = new TasksDTO
            {
                Id = actual.Id,
                Description = actual.Description,
                CreatedAt = actual.CreatedAt,
                FinishedAt = new DateTime(),
                Name = actual.Name,
                PerformerId = 1,
                ProjectId = actual.ProjectId,
                State = TaskStateDTO.IsProgress
            };

            var expected = await _tasksService.UpdateTask(actual);


            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void CreateTask_ThanDeleteTask_ThanGetTask()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _tasksService = new TasksService(_unitOf, _mapper);

            //Act
            var task = await _tasksService.CreateTask(_newTask);
            _tasksService.DeleteTask(task.Id);
            var actual = await _tasksService.GetTask(task.Id);

            //Assert
            Assert.Null(actual);
        }

        [Fact]
        public async void GetAllTask_ThanCreateNewTask_ThanGetAllTasks()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _tasksService = new TasksService(_unitOf, _mapper);

            //Act
            var expected = await _tasksService.GetAllTasks();
            var task = await _tasksService.CreateTask(_newTask);
            var actual = await _tasksService.GetAllTasks();

            //Assert
            Assert.Equal(expected.Count() + 1, actual.Count());
        }
    }
}
