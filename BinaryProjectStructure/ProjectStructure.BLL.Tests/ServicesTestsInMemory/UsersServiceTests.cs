﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class UsersServiceTests
    {
        private readonly NewUserDTO _newUser = new()
        {
            FirstName = "Max",
            LastName = "Khrapal",
            BirthDay = new(1982, 10, 22, 0, 0, 0),
            Email = "test@test.com.ua"
        };

        private IUnitOfWork _unitOf;
        private IUsersService _usersService;
        private IMapper _mapper;

        private readonly MapperConfiguration _mapperConfiguration = new(cfg =>
        {
            cfg.CreateMap<NewUserDTO, User>();
            cfg.CreateMap<UserDTO, User>();
            cfg.CreateMap<User, UserDTO>();
        });

        private readonly DbContextOptions<ProjectContext> _options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase("TestUser")
                .Options;

        [Fact]
        public async void CreateUser_ThanGetUser()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _usersService = new UsersService(_unitOf, _mapper);

            //Act
            var expected = await _usersService.CreateUser(_newUser);
            var actual = await _usersService.GetUser(expected.Id);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void CreateUser_ThanUpdateUser()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _usersService = new UsersService(_unitOf, _mapper);

            //Act
            var actual = await _usersService.CreateUser(_newUser);

            actual = new UserDTO
            {
                Id = actual.Id,
                FirstName = actual.FirstName,
                LastName = actual.LastName,
                BirthDay = actual.BirthDay,
                Email = "Max@test.com.ua",
                RegisteredAt = actual.RegisteredAt,
                TeamId = 1
            };
            var expected = await _usersService.UpdateUser(actual);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void CreateUser_ThanDeleteUser_ThanGetUser()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _usersService = new UsersService(_unitOf, _mapper);

            //Act
            var user = await _usersService.CreateUser(_newUser);
            _usersService.DeleteUser(user.Id);
            var actual = await _usersService.GetUser(user.Id);

            //Assert
            Assert.Null(actual);
        }

        [Fact]
        public async void GetAllUsers_ThanCreateNewUser_ThanGetAllUsers()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _usersService = new UsersService(_unitOf, _mapper);

            //Act
            var expected = await _usersService.GetAllUsers();
            var user = await _usersService.CreateUser(_newUser);
            var actual = await _usersService.GetAllUsers();

            //Assert
            Assert.Equal(expected.Count() + 1, actual.Count());
        }       
    }
}
