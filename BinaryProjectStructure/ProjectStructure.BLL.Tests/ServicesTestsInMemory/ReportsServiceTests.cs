using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class ReportsServiceTests
    {
        private readonly NewUserDTO _newUserMax = new()
        {
            FirstName = "Max",
            LastName = "Khrapal",
            BirthDay = new(1982, 10, 22, 0, 0, 0),
            Email = "test@test.com.ua"
        };

        private readonly NewUserDTO _newUserIrina = new()
        {
            FirstName = "Irina",
            LastName = "Khrapal",
            BirthDay = new(1985, 9, 4, 0, 0, 0),
            Email = "test@test.com.ua"
        };

        private readonly NewProjectDTO _newProjectFirst = new()
        {
            AuthorId = 0,
            Name = "First project",
            Description = "Project",
            Deadline = new DateTime().AddMonths(3)
        };

        private readonly NewProjectDTO _newProjectSecond = new()
        {
            AuthorId = 0,
            Name = "Second project",
            Description = "Project",
            Deadline = new DateTime().AddMonths(3)
        };

        private readonly NewTaskDTO _newTaskFirst = new()
        {
            ProjectId = 0,
            Name = "First task",
            Description = "Description"
        };

        private readonly NewTaskDTO _newTaskSecond = new()
        {
            ProjectId = 0,
            Name = "Second task",
            Description = "Description"
        };

        private readonly NewTeamDTO _newTeamFirst = new()
        {
            Name = "First team"
        };

        private IReportsService _reportsService;
        private IProjectService _projectService;
        private IUsersService _usersService;
        private ITasksService _tasksService;
        private ITeamService _teamService;
        private IUnitOfWork _unitOf;
        private IMapper _mapper;

        private readonly MapperConfiguration _mapperConfiguration = new(cfg =>
        {
            cfg.CreateMap<NewTeamDTO, Team>();
            cfg.CreateMap<TeamDTO, Team>();
            cfg.CreateMap<Team, TeamDTO>();

            cfg.CreateMap<NewUserDTO, User>();
            cfg.CreateMap<UserDTO, User>();
            cfg.CreateMap<User, UserDTO>();

            cfg.CreateMap<NewProjectDTO, Project>();
            cfg.CreateMap<ProjectDTO, Project>();
            cfg.CreateMap<Project, ProjectDTO>();

            cfg.CreateMap<NewTaskDTO, Tasks>();
            cfg.CreateMap<TasksDTO, Tasks>();
            cfg.CreateMap<Tasks, TasksDTO>();
        });

        private readonly DbContextOptions<ProjectContext> _options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase("TestReport")
                .Options;

        [Fact]
        public async void GetAllTasksInProjectsByAuthor()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _projectService = new ProjectService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);

            var user = await _usersService.CreateUser(_newUserMax);
            var project = await _projectService.CreateProject(_newProjectFirst);

            project = new ProjectDTO()
            {
                Id = project.Id,
                AuthorId = user.Id,
                CreatedAt = project.CreatedAt,
                Deadline = project.Deadline,
                Description = project.Description,
                Name = project.Name,
                TeamId = project.TeamId
            };

            var updatedProject = await _projectService.UpdateProject(project);

            var taskFirst = await _tasksService.CreateTask(_newTaskFirst);
            var taskSecond = await _tasksService.CreateTask(_newTaskSecond);

            taskFirst = new TasksDTO()
            {
                Id = taskFirst.Id,
                CreatedAt = taskFirst.CreatedAt,
                Description = taskFirst.Description,
                FinishedAt = taskFirst.FinishedAt,
                Name = taskFirst.Name,
                PerformerId = user.Id,
                State = TaskStateDTO.IsProgress,
                ProjectId = updatedProject.Id
            };
            taskSecond = new TasksDTO()
            {
                Id = taskSecond.Id,
                CreatedAt = taskSecond.CreatedAt,
                Description = taskSecond.Description,
                FinishedAt = taskSecond.FinishedAt,
                Name = taskSecond.Name,
                PerformerId = user.Id,
                State = TaskStateDTO.IsProgress,
                ProjectId = updatedProject.Id
            };

            await _tasksService.UpdateTask(taskFirst);
            await _tasksService.UpdateTask(taskSecond);

            var expected = new List<Tuple<ProjectDTO, int>>();
            var tuple = (project, count: 2).ToTuple();
            expected.Add(tuple);

            //Act
            var actual = await _reportsService.GetAllTasksInProjectsByAuthor(user.Id);

            //Assert
            Assert.Equal(expected.Select(x => x.Item1), actual.Select(x => x.Item1));
            Assert.Equal(expected.Select(x => x.Item2), actual.Select(x => x.Item2));
        }

        [Fact]
        public async void GetAllTasksOnPerformer()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _projectService = new ProjectService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);
           
            var project = await _projectService.CreateProject(_newProjectFirst);
            var task = await _tasksService.CreateTask(_newTaskFirst);

            int userId = 1;
            task = new TasksDTO()
            {
                Id = task.Id,
                ProjectId = task.ProjectId,
                CreatedAt = task.CreatedAt,
                Description = task.Description,
                FinishedAt = task.FinishedAt,
                Name = task.Name,
                State = TaskStateDTO.IsProgress,
                PerformerId = userId
            };

            await _tasksService.UpdateTask(task);
            var tasks = await _tasksService.GetAllTasks();

            int lengthNameTask = 45;
            var expected = tasks.Where(x => x.PerformerId == userId & x.Name.Length < lengthNameTask).Count();

            //Act   
            var actual = await _reportsService.GetAllTasksOnPerformer(userId);
            int count = actual.Count;

            //Assert
            Assert.Equal(expected, count);
        }

        [Fact]
        public async void GetAllTasksThatFinished()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _projectService = new ProjectService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);

            var user = await _usersService.CreateUser(_newUserMax);
            var project = await _projectService.CreateProject(_newProjectFirst);
            var task = await _tasksService.CreateTask(_newTaskFirst);

            task = new TasksDTO()
            {
                Id = task.Id,
                ProjectId = task.ProjectId,
                CreatedAt = task.CreatedAt,
                Description = task.Description,
                FinishedAt = new DateTime(2021, 7, 5),
                Name = task.Name,
                State = TaskStateDTO.IsDone,
                PerformerId = user.Id
            };

            var updatedTask = await _tasksService.UpdateTask(task);

            var expected = new List<Tuple<int, string>>();
            var tuple = (id: updatedTask.Id, name: updatedTask.Name).ToTuple();
            expected.Add(tuple);

            //Act
            var actual = await _reportsService.GetAllTasksThatFinished(user.Id);

            //Assert
            Assert.Equal(expected.Select(x => x.Item1), actual.Select(x => x.Item1));
            Assert.Equal(expected.Select(x => x.Item2), actual.Select(x => x.Item2));
        }

        [Fact]
        public async void GetAllUsersOldestThanTenYears()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _teamService = new TeamService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);

            var userFirst = await _usersService.CreateUser(_newUserMax);
            var userSecond = await _usersService.CreateUser(_newUserIrina);

            var createdTeamFirst = await _teamService.CreateTeam(_newTeamFirst);

            userFirst = new UserDTO()
            {
                Id = userFirst.Id,
                BirthDay = userFirst.BirthDay,
                Email = userFirst.Email,
                FirstName = userFirst.FirstName,
                LastName = userFirst.LastName,
                RegisteredAt = new(2020, 7, 19),
                TeamId = createdTeamFirst.Id
            };
            userSecond = new UserDTO()
            {
                Id = userSecond.Id,
                BirthDay = userSecond.BirthDay,
                Email = userSecond.Email,
                FirstName = userSecond.FirstName,
                LastName = userSecond.LastName,
                RegisteredAt = new(2021, 7, 19),
                TeamId = createdTeamFirst.Id
            };

            var updatedUserFirst = await _usersService.UpdateUser(userFirst);
            var updatedUserSecond = await _usersService.UpdateUser(userSecond);

            int ageUser = 10;
            var teams = await _teamService.GetAllTeams();
            var users = await _usersService.GetAllUsers();
            var actualUsers = users.Where(x => x.BirthDay.AddYears(ageUser) < DateTime.Now).OrderByDescending(x => x.RegisteredAt);

            var expected = new List<Tuple<int, string, List<UserDTO>>>();

            foreach (var team in teams)
            {
                var tuple = (team.Id, team.Name, actualUsers.Where(x => x.TeamId == team.Id).ToList()).ToTuple();
                expected.Add(tuple);
            }

            //Act
            var actual = await _reportsService.GetAllUsersOldestThanTenYears();

            //Assert
            Assert.Equal(expected.Select(x => x.Item1), actual.Select(x => x.Item1));
            Assert.Equal(expected.Select(x => x.Item2), actual.Select(x => x.Item2));
            Assert.Equal(expected.Select(x => x.Item3.Count), actual.Select(x => x.Item3.Count));
            Assert.Equal(expected.Select(x => x.Item3.Select(x => x.Id)), actual.Select(x => x.Item3.Select(x => x.Id)));
        }

        [Fact]
        public async void SortAllUsersFirstNameAndSortTaskOnName()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);

            var userFirst = await _usersService.CreateUser(_newUserMax);
            var userSecond = await _usersService.CreateUser(_newUserIrina);

            userFirst = new UserDTO()
            {
                Id = userFirst.Id,
                BirthDay = userFirst.BirthDay,
                Email = userFirst.Email,
                FirstName = userFirst.FirstName,
                LastName = userFirst.LastName,
                RegisteredAt = new(2020, 7, 19),
                TeamId = userFirst.Id
            };
            userSecond = new UserDTO()
            {
                Id = userSecond.Id,
                BirthDay = userSecond.BirthDay,
                Email = userSecond.Email,
                FirstName = userSecond.FirstName,
                LastName = userSecond.LastName,
                RegisteredAt = new(2021, 7, 19),
                TeamId = userSecond.Id
            };

            var updatedUserFirst = await _usersService.UpdateUser(userFirst);
            var updatedUserSecond = await _usersService.UpdateUser(userSecond);

            var users = await _usersService.GetAllUsers();
            var actualUsers = users.OrderBy(x => x.FirstName);

            var taskFirst = await _tasksService.CreateTask(_newTaskFirst);
            var taskSecond = await _tasksService.CreateTask(_newTaskSecond);

            taskFirst = new TasksDTO()
            {
                Id = taskFirst.Id,
                ProjectId = taskFirst.ProjectId,
                CreatedAt = taskFirst.CreatedAt,
                Description = taskFirst.Description,
                FinishedAt = new DateTime(2021, 7, 5),
                Name = taskFirst.Name,
                State = TaskStateDTO.IsDone,
                PerformerId = updatedUserFirst.Id
            };
            taskSecond = new TasksDTO()
            {
                Id = taskSecond.Id,
                ProjectId = taskSecond.ProjectId,
                CreatedAt = taskSecond.CreatedAt,
                Description = taskSecond.Description,
                FinishedAt = new DateTime(2021, 7, 5),
                Name = taskSecond.Name,
                State = TaskStateDTO.IsDone,
                PerformerId = updatedUserFirst.Id
            };

            await _tasksService.UpdateTask(taskFirst);
            await _tasksService.UpdateTask(taskSecond);

            var tasks = await _tasksService.GetAllTasks();
            var actualTasks = tasks.OrderByDescending(x => x.Name.Length);

            var expected = new List<Tuple<string, List<TasksDTO>>>();

            foreach (var user in actualUsers)
            {
                var tuple = (user.FirstName, actualTasks.Where(x => x.PerformerId == user.Id).ToList()).ToTuple();
                if (actualTasks.Where(x => x.PerformerId == user.Id).ToList().Count != 0)
                {
                    expected.Add(tuple);
                }
            }

            //Act
            var actual = await _reportsService.SortAllUsersFirstNameAndSortTaskOnName();

            //Assert
            Assert.Equal(expected.Select(x => x.Item1), actual.Select(x => x.Item1));
            Assert.Equal(expected.Select(x => x.Item2.Count), actual.Select(x => x.Item2.Count));
            Assert.Equal(expected.Select(x => x.Item2.Select(x => x.Name)), actual.Select(x => x.Item2.Select(x => x.Name)));
        }

        [Fact]
        public async void GetStructUserById()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _projectService = new ProjectService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);

            var user = await _usersService.CreateUser(_newUserMax);
            var projectFirst = await _projectService.CreateProject(_newProjectFirst);
            var projectSecond = await _projectService.CreateProject(_newProjectSecond);
            var taskFirst = await _tasksService.CreateTask(_newTaskFirst);
            var taskSecond = await _tasksService.CreateTask(_newTaskSecond);

            projectFirst = new ProjectDTO()
            {
                Id = projectFirst.Id,
                AuthorId = user.Id,
                CreatedAt = new(2020, 7, 10),
                Deadline = projectFirst.Deadline,
                Description = projectFirst.Description,
                Name = projectFirst.Name,
                TeamId = projectFirst.TeamId
            };
            projectSecond = new ProjectDTO()
            {
                Id = projectSecond.Id,
                AuthorId = user.Id,
                CreatedAt = new(2021, 7, 10),
                Deadline = projectSecond.Deadline,
                Description = projectSecond.Description,
                Name = projectSecond.Name,
                TeamId = projectSecond.TeamId
            };

            await _projectService.UpdateProject(projectFirst);
            await _projectService.UpdateProject(projectSecond);

            taskFirst = new TasksDTO()
            {
                Id = taskFirst.Id,
                ProjectId = projectFirst.Id,
                CreatedAt = new(2020, 7, 5),
                Description = taskFirst.Description,
                FinishedAt = new(2021, 7, 5),
                Name = taskFirst.Name,
                State = TaskStateDTO.IsDone,
                PerformerId = user.Id
            };
            taskSecond = new TasksDTO()
            {
                Id = taskSecond.Id,
                ProjectId = projectFirst.Id,
                CreatedAt = taskSecond.CreatedAt,
                Description = taskSecond.Description,
                FinishedAt = taskSecond.FinishedAt,
                Name = taskSecond.Name,
                State = TaskStateDTO.IsDone,
                PerformerId = taskSecond.Id
            };

            await _tasksService.UpdateTask(taskFirst);
            await _tasksService.UpdateTask(taskSecond);

            var expectedUser = _mapper.Map<User>(await _usersService.GetUser(user.Id));
            var expectedProject = _mapper.Map<Project>(_projectService.GetAllProjects().Result.Where(x => x.AuthorId == expectedUser.Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault() ?? null);

            int expectedCountTasksInProject = 0;
            var date = new DateTime(0001, 01, 01, 00, 00, 00);

            if (expectedProject != null)
            {
                expectedCountTasksInProject = _tasksService.GetAllTasks().Result.Where(x => x.ProjectId == expectedProject.Id).Where(x => x.CreatedAt != date).Count();
            }

            int expectedCountTasksInWork = _tasksService.GetAllTasks().Result.Count(x => x.PerformerId == user.Id & x.FinishedAt != date);

            var expectedTask = _mapper.Map<Tasks>(_tasksService.GetAllTasks().Result.Where(x => x.PerformerId == user.Id).OrderByDescending(x => x.FinishedAt - x.CreatedAt).FirstOrDefault() ?? null);

            var expected = new UserInfo()
            {
                User = expectedUser,
                Project = expectedProject,
                CountTasksInProject = expectedCountTasksInProject,
                CountTasksInWork = expectedCountTasksInWork,
                Tasks = expectedTask
            };

            //Act
            var actual = await _reportsService.GetStructUserById(user.Id);

            //Assert
            Assert.Equal(expected.User.Id, actual.Value.User.Id);
            Assert.Equal(expected.Project.Id, actual.Value.Project.Id);
            Assert.Equal(expected.CountTasksInProject, actual.Value.CountTasksInProject);
            Assert.Equal(expected.CountTasksInWork, actual.Value.CountTasksInWork);
            Assert.Equal(expected.Tasks.Id, actual.Value.Tasks.Id);
        }

        [Fact]
        public async void GetStructAllProjects()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _reportsService = new ReportsService(_unitOf, _mapper);
            _projectService = new ProjectService(_unitOf, _mapper);
            _usersService = new UsersService(_unitOf, _mapper);
            _tasksService = new TasksService(_unitOf, _mapper);
            _teamService = new TeamService(_unitOf, _mapper);

            var createTeamFirst = new Team()
            {
                Id = 0,
                Name = "First team",
                CreatedAt = DateTime.Now
            };
            var createTeamSecond = new Team()
            {
                Id = 0,
                Name = "Second team",
                CreatedAt = DateTime.Now
            };
            _contextMemory.Teams.Add(createTeamFirst);
            _contextMemory.Teams.Add(createTeamSecond);

            var createUserFirst = new User()
            {
                Id = 0,
                FirstName = "Irina",
                LastName = "Khrapal",
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now,
                Email = "test@test.com.ua",
                TeamId = 1,
            };
            var createUserSecond = new User()
            {
                Id = 0,
                FirstName = "Max",
                LastName = "Khrapal",
                BirthDay = DateTime.Now,
                RegisteredAt = DateTime.Now,
                Email = "test@test.com.ua",
                TeamId = 2,
            };
            _contextMemory.Users.Add(createUserFirst);
            _contextMemory.Users.Add(createUserSecond);

            var createProjectFirst = new Project()
            {
                Id = 0,
                AuthorId = 1,
                Deadline = DateTime.Now.AddMonths(3),
                CreatedAt = DateTime.Now,
                Description = "This is the longest description of the project",
                Name = "First project with long descriprion",
                TeamId = 1,
            };
            var createProjectSecond = new Project()
            {
                Id = 0,
                AuthorId = 1,
                Deadline = DateTime.Now.AddMonths(3),
                CreatedAt = DateTime.Now,
                Description = "This is the shot description of the project",
                Name = "Second project with shot descriprion",
                TeamId = 2,
            };
            var createProjectThird = new Project()
            {
                Id = 0,
                AuthorId = 1,
                Deadline = DateTime.Now.AddMonths(3),
                CreatedAt = DateTime.Now,
                Description = "Description of the project",
                Name = "Third project",
                TeamId = 2,
            };
            _contextMemory.Projects.Add(createProjectFirst);
            _contextMemory.Projects.Add(createProjectSecond);

            var createTaskFirst = new Tasks()
            {
                Id = 0,
                Description = "This is the longest description of the task",
                CreatedAt = DateTime.Now,
                FinishedAt = null,
                Name = "taskFirst",
                PerformerId = 1,
                ProjectId = 1,
                State = 0
            };
            var createTaskSecond = new Tasks()
            {
                Id = 0,
                Description = "Description taskSecond",
                CreatedAt = DateTime.Now,
                FinishedAt = null,
                Name = "taskSecond",
                PerformerId = 1,
                ProjectId = 1,
                State = 0
            };
            var createTaskThird = new Tasks()
            {
                Id = 0,
                Description = "Description taskThird",
                CreatedAt = DateTime.Now,
                FinishedAt = null,
                Name = "taskThird",
                PerformerId = 2,
                ProjectId = 2,
                State = 0
            };
            var createTaskFour = new Tasks()
            {
                Id = 0,
                Description = "Description taskFour",
                CreatedAt = DateTime.Now,
                FinishedAt = null,
                Name = "taskFour",
                PerformerId = 2,
                ProjectId = 2,
                State = 0
            };
            var createTaskFive = new Tasks()
            {
                Id = 0,
                Description = "Description taskFive",
                CreatedAt = DateTime.Now,
                FinishedAt = null,
                Name = "taskFive",
                PerformerId = 2,
                ProjectId = 2,
                State = 0
            };
            _contextMemory.Tasks.Add(createTaskFirst);
            _contextMemory.Tasks.Add(createTaskSecond);
            _contextMemory.Tasks.Add(createTaskThird);
            _contextMemory.Tasks.Add(createTaskFour);
            _contextMemory.Tasks.Add(createTaskFive);

            var projects = await _projectService.GetAllProjects();
            var tasks = await _tasksService.GetAllTasks();

            var expected = new List<ProjectInfo>();

            int descriptionLength = 20;
            int countTask = 3;
            int countUsers = 0;
            foreach (var project in projects)
            {

                foreach (var task in tasks)
                {
                    if (task.ProjectId == project.Id & task.PerformerId != 0)
                        countUsers++;
                }

                expected.Add(
                    new ProjectInfo()
                    {
                        Project = _mapper.Map<Project>(project),

                        TaskLongDescription = _mapper.Map<Tasks>(_tasksService.GetAllTasks().Result
                        .Where(x => x.ProjectId == project.Id).OrderBy(x => x.Description.Length).FirstOrDefault() ?? new TasksDTO()),

                        TasksShortName = _mapper.Map<Tasks>(_tasksService.GetAllTasks().Result
                        .Where(x => x.ProjectId == project.Id).OrderBy(x => x.Name.Length).FirstOrDefault() ?? new TasksDTO()),

                        CountUsersInTeamProject = project.Description.Length > descriptionLength | _tasksService.GetAllTasks().Result
                        .Where(x => x.ProjectId == project.Id).Count() < countTask ? countUsers : 0
                    });

                countUsers = 0;
            }

            //Act
            var actual = await _reportsService.GetStructAllProjects();

            //Assert
            Assert.Equal(expected.Select(x => x.Project).Count(), actual.Select(x => x.Project).Count());
            Assert.Equal(expected, actual);
        }
    }
}
