﻿using FakeItEasy;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class RepositoryTests
    {
        private readonly User _newUser = new()
        {
            Id = 0,
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3",
            RegisteredAt = DateTime.Now,
            TeamId = 2
        };

        private readonly User _user = new()
        {
            Id = 3,
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3",
            RegisteredAt = DateTime.Now,
            TeamId = 2
        };

        private readonly List<User> _users = new()
        {
            new User() { Id = 1, BirthDay = DateTime.Now, Email = "test1@test", FirstName = "user1", LastName = "user1", RegisteredAt = DateTime.Now, TeamId = 1 },
            new User() { Id = 2, BirthDay = DateTime.Now, Email = "test2@test", FirstName = "user2", LastName = "user2", RegisteredAt = DateTime.Now, TeamId = 1 }
        };

        private readonly NewUserDTO _newUserDTO = new()
        {
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3"
        };

        private readonly UserDTO _userDTO = new()
        {
            Id = 3,
            BirthDay = DateTime.Now,
            Email = "test3@test.ua",
            FirstName = "user3",
            LastName = "user3",
            RegisteredAt = DateTime.Now,
            TeamId = 2
        };

        private readonly List<UserDTO> _usersDTO = new()
        {
            new UserDTO() { Id = 1, BirthDay = DateTime.Now, Email = "test1@test", FirstName = "user1", LastName = "user1", RegisteredAt = DateTime.Now, TeamId = 1 },
            new UserDTO() { Id = 2, BirthDay = DateTime.Now, Email = "test2@test", FirstName = "user2", LastName = "user2", RegisteredAt = DateTime.Now, TeamId = 1 }
        };

        private readonly IRepository<object> _uow;

        public RepositoryTests()
        {
            _uow = A.Fake<IRepository<object>>();
        }

        [Fact]
        public void GetAllUsers_InRepository()
        {
            //Arrange      
            A.CallTo(() => _uow.GetAll()).Returns(_users);

            //Act
            var actual = _uow.GetAll().Count();

            //Assert
            Assert.Equal(_users.Count, actual);
        }

        [Fact]
        public void GetUser_InRepository()
        {
            //Arrange      
            int id = 2;
            A.CallTo(() => _uow.Get(id)).Returns(_users.Find(x => x.Id == id));

            //Act
            var expected = _uow.Get(id);

            //Assert
            Assert.Contains(expected, _users);
        }

        [Fact]
        public void CreateUser_InRepository()
        {
            //Arrange   
            A.CallTo(() => _uow.Create(_newUser)).Returns(_user);

            //Act
            var actual = _uow.Create(_newUser);

            //Assert
            Assert.Equal(_user, actual);
            Assert.NotEqual(_newUser, actual);
        }

        [Fact]
        public void UpdateUser_InRepository()
        {
            //Arrange
            int id = 2;

            //Act
            _uow.Update(id, _user);

            //Assert
            A.CallTo(() => _uow.Update(id, _user)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void DeleteUser_InRepository()
        {
            //Arrange
            int id = 2;

            //Act
            _uow.Delete(id);

            //Assert
            A.CallTo(() => _uow.Delete(id)).MustHaveHappenedOnceExactly();
        }   
    }
}
