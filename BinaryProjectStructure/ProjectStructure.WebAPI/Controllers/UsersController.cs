﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetAllUsers()
        {
            return Ok(await _usersService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUser(int id)
        {
            var user = await _usersService.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> CreateUser([FromBody] NewUserDTO newUser)
        {
            return Ok(await _usersService.CreateUser(newUser));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<UserDTO>> UpdateUser(int id, [FromBody] UserDTO user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            var userToUpdate = await _usersService.GetUser(id);

            if (userToUpdate == null)
            {
                return NotFound();
            }

            return Ok(await _usersService.UpdateUser(user));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            var userToDelete = await _usersService.GetUser(id);

            if (userToDelete == null)
            {
                return NotFound();
            }

            _usersService.DeleteUser(id);
            return Ok();
        }
    }
}
