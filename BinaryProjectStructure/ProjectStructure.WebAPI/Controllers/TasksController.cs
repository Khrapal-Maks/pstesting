﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetAllTasks()
        {
            return Ok(await _tasksService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetTask(int id)
        {
            var task = await _tasksService.GetTask(id);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        [HttpPost]
        public async Task<ActionResult<TasksDTO>> CreateTask([FromBody] NewTaskDTO newTask)
        {
            return Ok(await _tasksService.CreateTask(newTask));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TasksDTO>> UpdateTask(int id, [FromBody] TasksDTO task)
        {
            if (id != task.Id)
            {
                return BadRequest();
            }

            var taskToUpdate = await _tasksService.GetTask(id);

            if (taskToUpdate == null)
            {
                return NotFound();
            }

            return Ok(await _tasksService.UpdateTask(task));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTask(int id)
        {
            var taskToDelete = await _tasksService.GetTask(id);

            if (taskToDelete == null)
            {
                return NotFound();
            }

            _tasksService.DeleteTask(id);
            return Ok();
        }
    }
}
