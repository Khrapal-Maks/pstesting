﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Tasks, TasksDTO>();

            CreateMap<TasksDTO, Tasks>();

            CreateMap<NewTaskDTO, Tasks>();
        }
    }
}
