﻿using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class NewTeamDTO
    {
        [Required]
        public string Name { get; set; }
    }
}
