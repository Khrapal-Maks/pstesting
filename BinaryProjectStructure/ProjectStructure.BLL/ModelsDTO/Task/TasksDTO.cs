﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class TasksDTO
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int PerformerId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public TaskStateDTO State { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }

        public override bool Equals(object obj)
        {
            return obj is TasksDTO dTO &&
                   Id == dTO.Id &&
                   ProjectId == dTO.ProjectId &&
                   PerformerId == dTO.PerformerId &&
                   Name == dTO.Name &&
                   Description == dTO.Description &&
                   State == dTO.State &&
                   CreatedAt == dTO.CreatedAt &&
                   FinishedAt == dTO.FinishedAt;
        }
    }
}
