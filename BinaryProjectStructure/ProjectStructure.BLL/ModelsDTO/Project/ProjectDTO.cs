﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class ProjectDTO
    {
        public int Id { get; set; }

        public int AuthorId { get; set; }

        public int TeamId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime Deadline { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ProjectDTO dTO &&
                   Id == dTO.Id &&
                   AuthorId == dTO.AuthorId &&
                   TeamId == dTO.TeamId &&
                   Name == dTO.Name &&
                   Description == dTO.Description &&
                   CreatedAt == dTO.CreatedAt &&
                   Deadline == dTO.Deadline;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, AuthorId, TeamId, Name, Description, CreatedAt, Deadline);
        }
    }
}
