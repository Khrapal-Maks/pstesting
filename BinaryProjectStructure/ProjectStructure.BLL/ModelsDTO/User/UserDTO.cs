﻿using System;

namespace ProjectStructure.BLL.ModelsDTO
{
    public sealed class UserDTO
    {
        public int Id { get; set; }

        public int? TeamId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime RegisteredAt { get; set; }

        public DateTime BirthDay { get; set; }

        public override bool Equals(object obj)
        {
            return obj is UserDTO dTO &&
                   Id == dTO.Id &&
                   TeamId == dTO.TeamId &&
                   FirstName == dTO.FirstName &&
                   LastName == dTO.LastName &&
                   Email == dTO.Email &&
                   RegisteredAt == dTO.RegisteredAt &&
                   BirthDay == dTO.BirthDay;
        }
    }
}
