﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService, IProjectService
    {
        private readonly IUnitOfWork _database;

        public ProjectService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            return await Task.Run(() =>
            {
                var projects = _database.GetRepositoryProjects.GetAll();

                return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
            });
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            return await Task.Run(() =>
            {
                var project = _database.GetRepositoryProjects.Get(id);

                return _mapper.Map<ProjectDTO>(project);
            });
        }

        public async Task<ProjectDTO> CreateProject(NewProjectDTO project)
        {
            return await Task.Run(() =>
            {
                var projectEntity = _mapper.Map<Project>(project);

                var createToProject = _database.GetRepositoryProjects.Create(projectEntity);
                _database.Save();

                var createdProject = _database.GetRepositoryProjects.Get(createToProject.Id);

                return _mapper.Map<ProjectDTO>(createdProject);
            });
        }

        public async Task<ProjectDTO> UpdateProject(ProjectDTO project)
        {
            return await Task.Run(() =>
            {
                var updateProject = _mapper.Map<Project>(project);

                _database.GetRepositoryProjects.Update(updateProject.Id, updateProject);
                _database.Save();

                var updatedProject = _database.GetRepositoryProjects.Get(project.Id);

                return _mapper.Map<ProjectDTO>(updatedProject);
            });
        }

        public void DeleteProject(int id)
        {
            _database.GetRepositoryProjects.Delete(id);
            _database.Save();
        }
    }
}
