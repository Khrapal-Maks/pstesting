﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class TeamService : BaseService, ITeamService
    {
        private readonly IUnitOfWork _database;

        public TeamService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            return await Task.Run(() =>
            {
                var teams = _database.GetRepositoryTeams.GetAll();

                return _mapper.Map<IEnumerable<TeamDTO>>(teams);
            });
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            return await Task.Run(() =>
            {
                var team = _database.GetRepositoryTeams.Get(id);

                return _mapper.Map<TeamDTO>(team);
            });
        }

        public async Task<TeamDTO> CreateTeam(NewTeamDTO newTeam)
        {
            return await Task.Run(() =>
            {
                var teamEntity = _mapper.Map<Team>(newTeam);

                var createToTeam = _database.GetRepositoryTeams.Create(teamEntity);
                _database.Save();

                var createdTeam = _database.GetRepositoryTeams.Get(createToTeam.Id);

                return _mapper.Map<TeamDTO>(createdTeam);
            });
        }

        public async Task<TeamDTO> UpdateTeam(TeamDTO team)
        {
            return await Task.Run(() =>
            {
                var updateTeam = _mapper.Map<Team>(team);

                _database.GetRepositoryTeams.Update(team.Id, updateTeam);
                _database.Save();

                var updatedTeam = _database.GetRepositoryTeams.Get(team.Id);

                return _mapper.Map<TeamDTO>(updatedTeam);
            });
        }

        public void DeleteTeam(int id)
        {
            _database.GetRepositoryTeams.Delete(id);
            _database.Save();
        }
    }
}
