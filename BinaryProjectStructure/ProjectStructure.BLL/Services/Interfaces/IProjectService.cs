﻿using ProjectStructure.BLL.ModelsDTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetAllProjects();

        Task<ProjectDTO> CreateProject(NewProjectDTO item);

        Task<ProjectDTO> GetProject(int value);

        Task<ProjectDTO> UpdateProject(ProjectDTO item);

        void DeleteProject(int value);
    }
}
