﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IUnitOfWork _database;

        public UsersService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            return await Task.Run(() =>
            {
                var users = _database.GetRepositoryUsers.GetAll();

                return _mapper.Map<IEnumerable<UserDTO>>(users);
            });
        }

        public async Task<UserDTO> GetUser(int id)
        {
            return await Task.Run(() =>
            {
                var user = _database.GetRepositoryUsers.Get(id);

                return _mapper.Map<UserDTO>(user);
            });
        }

        public async Task<UserDTO> CreateUser(NewUserDTO newUser)
        {
            return await Task.Run(() =>
            {
                var userEntity = _mapper.Map<User>(newUser);

                var createToUser = _database.GetRepositoryUsers.Create(userEntity);
                _database.Save();

                var createdUser = _database.GetRepositoryUsers.Get(createToUser.Id);

                return _mapper.Map<UserDTO>(createdUser);
            });
        }

        public async Task<UserDTO> UpdateUser(UserDTO user)
        {
            return await Task.Run(() =>
            {
                var updateUser = _mapper.Map<User>(user);

                _database.GetRepositoryUsers.Update(user.Id, updateUser);
                _database.Save();

                var updatedUser = _database.GetRepositoryUsers.Get(user.Id);

                return _mapper.Map<UserDTO>(updatedUser);
            });
        }

        public void DeleteUser(int id)
        {
            _database.GetRepositoryUsers.Delete(id);
            _database.Save();
        }
    }
}
