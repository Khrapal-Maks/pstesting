﻿using AutoMapper;
using ProjectStructure.DAL.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public abstract class BaseService
    {
        private protected readonly IUnitOfWork _context;
        private protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
